/**
 * 
 */
package sim.math;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import sim.exception.SNoImplementationException;
import sim.util.SLog;

/**
 * JUnit test permettant de valider les fonctionnalit�s de la classe <b>SDiscreteFunction</b>.
 * 
 * @author Simon V�zina
 * @since 2018-03-30
 * @version 2018-12-12
 */
public class SDiscreteFunctionTest {

  /**
   * Test de la m�thode <b>dx</b> dans l'exemple situ� dans le laboratoire : Les fonctions discr�tes.
   */
  @Test
  public void dxTest1()
  {
    try{
      
      double x_min = 0.0;
      double x_max = 2.0;
      double[] function = new double[6];
      
      double expected = 0.4;
      
      Assert.assertEquals(expected, SDiscreteFunction.dx(function, x_min, x_max), 0.0001);
      
    }catch(SNoImplementationException e){ 
      SLog.logWriteLine("SDiscreteFunctionTest ---> Test non effectu� : public void dxTest1()");
    }
  }
  
  /**
   * Test de la m�thode <b>dx</b> dans un cas quelconque.
   */
  @Test
  public void dxTest2()
  {
    try{
      
      double x_min = 3.0;
      double x_max = 10.0;
      double[] function = new double[24];
      
      double expected = 0.304347826;
      
      Assert.assertEquals(expected, SDiscreteFunction.dx(function, x_min, x_max), 0.0001);
      
    }catch(SNoImplementationException e){ 
      SLog.logWriteLine("SDiscreteFunctionTest ---> Test non effectu� : public void dxTest2()");
    }  
  }
  
  /**
   * Test de la m�thode <b>f</b> lorsque x = x_min et x = x_max.
   */
  @Test
  public void fTest1()
  {
    try{
      double[] function = { 1.0, 2.0, 3.0, 4.0, 5.0 };
      double x_min = 0.0;
      double x_max = 10.0;
          
      Assert.assertEquals(1.0, SDiscreteFunction.f(0.0, function, x_min, x_max), 0.001);
      Assert.assertEquals(5.0, SDiscreteFunction.f(10.0, function, x_min, x_max), 0.001);
      
    }catch(SNoImplementationException e){ 
      SLog.logWriteLine("SDiscreteFunctionTest ---> Test non effectu� : public void fTest1()");
    } 
  }

  /**
   * Test de la m�thode <b>f</b> lorsque x est une valeur entre x_min et x_max.
   */
  @Test
  public void fTest2()
  {
    try{
      double[] function = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 };
      double x_min = 0.0;
      double x_max = 10.0;
          
      Assert.assertEquals(2.0, SDiscreteFunction.f(2.5, function, x_min, x_max), 0.001);
      Assert.assertEquals(5.0, SDiscreteFunction.f(8.5, function, x_min, x_max), 0.001);
      
    }catch(SNoImplementationException e){ 
      SLog.logWriteLine("SDiscreteFunctionTest ---> Test non effectu� : public void fTest2()");
    }
  }
  
  /**
   * Test de la m�thode <b>f</b> lorsque x est une valeur non valide.
   */
  @Test
  public void fTest3()
  {
    try{
      double[] function = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 };
      double x_min = 0.0;
      double x_max = 10.0;
          
      try {
        Assert.assertEquals(2.0, SDiscreteFunction.f(-2.0, function, x_min, x_max), 0.001);
        fail("Ce test est un �chec, car la fonction est �valu�e � l'ext�rieur du domaine.");
      }catch(IndexOutOfBoundsException e) {
        // c'est un succ�s !
      }
      
    }catch(SNoImplementationException e){ 
      SLog.logWriteLine("SDiscreteFunctionTest ---> Test non effectu� : public void fTest3()");
    }
  }
 
  /**
   * Test de la m�thode <b>rightDerivate</b>. Ce test permet de v�rifier que le tableau fx n'est pas modifi� durant l'�valuation de la d�riv�e.
   */
  @Test
  public void rightDerivateTest1()
  {
    try{
      
      double[] fx = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      double[] copy = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      double dx = 2.0;
      
      //-------------------------------
      // Effectuer la m�thode � tester.
      //-------------------------------
      SDiscreteFunction.rightDerivate(3, fx, dx);
           
      Assert.assertArrayEquals(copy, fx, 0.00001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void rightDerivateTest1()");
    } 
  }
  
  /**
   * Test de la m�thode <b>rightDerivate</b> dans un cas g�n�ral.
   */
  @Test
  public void rightDerivateTest2()
  {
    try{
      
      double[] fx = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      double dx = 2.0;
      
      //-------------------------------
      // Effectuer la m�thode � tester.
      //-------------------------------
      double calculated = SDiscreteFunction.rightDerivate(3, fx, dx);
      
      double expected = ( 7.0 - 2.0 ) / 2.0;
      
      Assert.assertEquals(expected, calculated, 0.00001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void rightDerivateTest2()");
    } 
  }
  
  /**
   * Test de la m�thode <b>leftDerivate</b>. Ce test permet de v�rifier que le tableau fx n'est pas modifi� durant l'�valuation de la d�riv�e.
   */
  @Test
  public void leftDerivateTest1()
  {
    try{
      
      double[] fx = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      double[] copy = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      
      double dx = 2.0;
      
      //-------------------------------
      // Effectuer la m�thode � tester.
      //-------------------------------
      SDiscreteFunction.leftDerivate(3, fx, dx);
           
      Assert.assertArrayEquals(copy, fx, 0.00001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void leftDerivateTest1()");
    } 
  }
  
  /**
   * Test de la m�thode <b>leftDerivate</b> dans un cas g�n�ral.
   */
  @Test
  public void leftDerivateTest2()
  {
    try{
      
      double[] fx = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      double dx = 2.0;
      
      //-------------------------------
      // Effectuer la m�thode � tester.
      //-------------------------------
      double calculated = SDiscreteFunction.leftDerivate(3, fx, dx);
      
      double expected = ( 2.0 - 3.0 ) / 2.0;
      
      Assert.assertEquals(expected, calculated, 0.00001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void leftDerivateTest2()");
    } 
  }
  
  /**
   * Test de la m�thode <b>middleDerivate</b>. Ce test permet de v�rifier que le tableau fx n'est pas modifi� durant l'�valuation de la d�riv�e.
   */
  @Test
  public void middleDerivateTest1()
  {
    try{
      
      double[] fx = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      double[] copy = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      double dx = 4.0;
      
      //-------------------------------
      // Effectuer la m�thode � tester.
      //-------------------------------
      SDiscreteFunction.middleDerivate(3, fx, dx);
           
      Assert.assertArrayEquals(copy, fx, 0.00001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void middleDerivateTest1()");
    } 
  }
  
  /**
   * Test de la m�thode <b>middleDerivate</b> en utilisant la moyenne de la d�riv�e � gauche et � droite.
   */
  @Test
  public void middleDerivateTest2()
  {
    try{
      
      double[] fx = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      double dx = 4.0;
      
      //-------------------------------
      // Effectuer la m�thode � tester.
      //-------------------------------
      double calculated = SDiscreteFunction.middleDerivate(1, fx, dx);
      
      double expected =  (3.0 - 5.0) / (2.0*4.0);
      
      Assert.assertEquals(expected, calculated, 0.00001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void middleDerivateTest2()");
    } 
  }
  
  /**
   * Test de la m�thode <b>middleDerivate</b> en utilisant la moyenne de la d�riv�e � gauche et � droite.
   */
  @Test
  public void middleDerivateTest3()
  {
    try{
      
      double[] fx = { 5.0, 9.0, 3.0, 2.0, 7.0, 1.0 };
      double dx = 2.0;
      
      //-------------------------------
      // Effectuer la m�thode � tester.
      //-------------------------------
      double calculated = SDiscreteFunction.middleDerivate(3, fx, dx);
      
      double left = SDiscreteFunction.leftDerivate(3, fx, dx);
      double right = SDiscreteFunction.rightDerivate(3, fx, dx);
      double expected =  (left + right) / 2.0;
      
      Assert.assertEquals(expected, calculated, 0.00001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void middleDerivateTest3()");
    } 
  }
  
  /**
   * Test de la m�thode <b>dfdx</b> pour un tableau � 3 �l�ments. 
   * On peut y v�rifier s'il y a respect du concept du calcul de la d�riv�e � gauche, au centre et � droite.
   */
  @Test
  public void dfdxTest1()
  {
    try{
      
      double x_min = 0.0;
      double x_max = 10.0;
      
      double[] f = { 5.7, 4.3, 8.9 };
      double dx = SDiscreteFunction.dx(f, x_min, x_max);
      
      // V�rification de la d�riv�e � droite.
      Assert.assertEquals(SDiscreteFunction.rightDerivate(0, f, dx), SDiscreteFunction.dfdx(0, f, dx), 0.0001);
      
      // V�rification de la d�riv�e au centre.
      Assert.assertEquals(SDiscreteFunction.middleDerivate(1, f, dx), SDiscreteFunction.dfdx(1, f, dx), 0.0001);
      
      // V�rification de la d�riv�e � gauche.
      Assert.assertEquals(SDiscreteFunction.leftDerivate(2, f, dx), SDiscreteFunction.dfdx(2, f, dx), 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void dfdxTest1()");
    } 
  }
  
  /**
   * Test de la m�thode <b>derivate</b> pour une fonction discr�te � 3 �l�ments. 
   * On peut y v�rifier s'il y a respect du concept du calcul de la d�riv�e � gauche, au centre et � droite.
   */
  @Test
  public void derivateTest1()
  {
    try{
      
      double x_min = 0.0;
      double x_max = 10.0;
      
      double[] f = { 5.7, 4.3, 8.9 };
      double dx = SDiscreteFunction.dx(f, x_min, x_max);
      
      double[] f_prime = SDiscreteFunction.derivate(f, x_min, x_max);
      
      // V�rification de la d�riv�e � droite.
      Assert.assertEquals(SDiscreteFunction.rightDerivate(0, f, dx), f_prime[0], 0.0001);
      
      // V�rification de la d�riv�e au centre.
      Assert.assertEquals(SDiscreteFunction.middleDerivate(1, f, dx), f_prime[1], 0.0001);
      
      // V�rification de la d�riv�e � gauche.
      Assert.assertEquals(SDiscreteFunction.leftDerivate(2, f, dx), f_prime[2], 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void derivateTest1()");
    } 
  }
  
  /**
   * Test de la m�thode <b>derivate</b> lorsque la fonction est une constante.
   */
  @Test
  public void derivateTest2()
  {
    try{
      
      double x_min = 0.0;
      double x_max = 10.0;
      
      // Construction de la fonction f : La fonction f(x) = C (constante).
      double[] f = new double[30];
      
      for(int i = 0; i < f.length; i++)
        f[i] = 5.8;
      
      double[] f_prime = SDiscreteFunction.derivate(f, x_min, x_max);
      
      // Calcul de la fonction attendue : La fonction f'(x) = 0 (nulle).
      double[] expected_f = new double[30];
      
      for(int i = 0; i < expected_f.length; i++)
        expected_f[i] = 0.0;
      
      // Validation.
      Assert.assertArrayEquals(expected_f, f_prime, 0.00001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void derivateTest2()");
    }
  }
  
  /**
   * Test de la m�thode <b>derivate</b> lorsque la fonction est lin�aire.
   */
  @Test
  public void derivateTest3()
  {
    try{
      
      double[] f = new double[30];
      
      double x_min = 0.0;
      double x_max = 10.0;
      
      double dx = SDiscreteFunction.dx(f, x_min, x_max);
      
      // Construction de la fonction f : La fonction f(x) = m*x + b (lin�aire).
      double m = 2.5;
      double b = 8.9;
      
      for(int i = 0; i < f.length; i++)
        f[i] = m*(x_min + i*dx) + b;
      
      double[] f_prime = SDiscreteFunction.derivate(f, x_min, x_max);
      
      // Calcul de la fonction attendue : La fonction f'(x) = m (constante).
      double[] expected_f = new double[30];
      
      for(int i = 0; i < expected_f.length; i++)
        expected_f[i] = m;
      
      // Validation.
      Assert.assertArrayEquals(expected_f, f_prime, 0.00001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void derivateTest3()");
    }
  }
  
  /**
   * Test de la m�thode <b>derivate</b> lorsque la fonction est sinus (la d�riv�e sera cosinus).
   */
  @Test
  public void derivateTest4()
  {
    try{
      
      double x_min = 0.0;
      double x_max = 10.0;
      
      double[] f = new double[3000];
            
      double dx = SDiscreteFunction.dx(f, x_min, x_max);
      
      // Construction de la fonction f : La fonction f(x) = sin(x).
      for(int i = 0; i < f.length; i++)
        f[i] = Math.sin(x_min + i*dx);
      
      double[] f_prime = SDiscreteFunction.derivate(f, x_min, x_max);
      
      // Calcul de la fonction attendue : La fonction f'(x) = cos(x).
      double[] expected_f = new double[3000];
      
      for(int i = 0; i < expected_f.length; i++)
        expected_f[i] = Math.cos(x_min + i*dx);
      
      // Validation au mili�me pr�s.
      Assert.assertArrayEquals(expected_f, f_prime, 0.001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void derivateTest4()");
    }
  }
  
  /**
   * Test de la m�thode <b>integrate</b> lorsque la fonction est nulle.
   */
  @Test
  public void integrateTest1()
  {
    try{
      
      double x_min = 0.0;
      double x_max = 10.0;
      
      double C = 6.7;
      
      double[] f = new double[30];
            
      // Construction de la fonction f : La fonction f(x) = 0.
      for(int i = 0; i < f.length; i++)
        f[i] = 0.0;
      
      double[] F = SDiscreteFunction.integrate(f, C ,x_min, x_max);
      
      // Calcul de la fonction attendue : La fonction F(x) = C.
      double[] expected_f = new double[30];
      
      for(int i = 0; i < expected_f.length; i++)
        expected_f[i] = C;
      
      // Validation au mili�me pr�s.
      Assert.assertArrayEquals(expected_f, F, 0.001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void integrateTest1()");
    }
  }
  
  /**
   * Test de la m�thode <b>integrate</b> lorsque la fonction est constante et d�fini sur l'intervalle [0, 10].
   */
  @Test
  public void integrateTest2a()
  {
    try{
      
      double x_min = 0.0;
      double x_max = 10.0;
      
      double C = 6.7;
      double m = 3.9;
      
      double[] f = new double[30];
            
      double dx = SDiscreteFunction.dx(f, x_min, x_max);
      
      // Construction de la fonction f : La fonction f(x) = m.
      for(int i = 0; i < f.length; i++)
        f[i] = m;
      
      double[] F = SDiscreteFunction.integrate(f, C ,x_min, x_max);
      
      // Calcul de la fonction attendue : La fonction F(x) = m*x + C.
      double[] expected_f = new double[30];
      
      for(int i = 0; i < expected_f.length; i++)
      {
        double x = i*dx;
        expected_f[i] = m*x + C;
      }
            
      // Validation au mili�me pr�s.
      Assert.assertArrayEquals(expected_f, F, 0.001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void integrateTest2a()");
    }
  }
  
  /**
   * Test de la m�thode <b>integrate</b> lorsque la fonction est constante et d�fini sur l'intervalle [10, 20].
   */
  @Test
  public void integrateTest2b()
  {
    try{
      
      double x_min = 10.0;
      double x_max = 20.0;
      
      double C = 6.7;
      double m = 3.9;
      
      double[] f = new double[30];
            
      double dx = SDiscreteFunction.dx(f, x_min, x_max);
      
      // Construction de la fonction f : La fonction f(x) = m.
      for(int i = 0; i < f.length; i++)
        f[i] = m;
      
      double[] F = SDiscreteFunction.integrate(f, C ,x_min, x_max);
      
      // Calcul de la fonction attendue : La fonction F(x) = m*x + C.
      double[] expected_f = new double[30];
      
      for(int i = 0; i < expected_f.length; i++)
      {
        double x = i*dx;
        expected_f[i] = m*x + C;
      }
            
      // Validation au mili�me pr�s.
      Assert.assertArrayEquals(expected_f, F, 0.001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void integrateTest2b()");
    }
  }
  
  /**
   * Test de la m�thode <b>integrate</b> sur la fonction sinue dans l'intervalle [0, 10].
   */
  @Test
  public void integrateTest3()
  {
    try{
      
      double x_min = 0.0;
      double x_max = 10.0;
      double C = -1.0;
      
      double[] f = new double[30];
            
      double dx = SDiscreteFunction.dx(f, x_min, x_max);
      
      // Construction de la fonction f : La fonction f(x) = sin(x).
      for(int i = 0; i < f.length; i++)
        f[i] = Math.sin(x_min + i*dx);
      
        
      double[] F = SDiscreteFunction.integrate(f, C ,x_min, x_max);
      
      // Calcul de la fonction attendue : La fonction F(x) = -cos(x).
      double[] expected_f = new double[30];
      
      for(int i = 0; i < expected_f.length; i++)
        expected_f[i] = -Math.cos(x_min + i*dx);
            
      // Validation.
      // Avec algorithme middleIntegrate, �a valide � 0.02
      // Avec algorithme simpsonIntegrate, �a valide � 0.0006
      // Avec algorithme simpsonAdvIntegrate, �a valide quand m�me � 0.0006.
      Assert.assertArrayEquals(expected_f, F, 0.0006);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void integrateTest3()");
    }
  }
  
}
