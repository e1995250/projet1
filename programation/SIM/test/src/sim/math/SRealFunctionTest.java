/**
 * 
 */
package sim.math;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import sim.exception.SConstructorException;
import sim.exception.SNoImplementationException;
import sim.util.SLog;

/**
 * <p>JUnit test permettant de valider les fonctionnalit�s de la classe <b>SRealFunction</b>.</p>
 * 
 * @author Simon V�zina
 * @since 2017-05-26
 * @version 2018-05-18
 */
public class SRealFunctionTest {

  /**
   * Test du constructeur dans le cas o� les bornes minimales et maximales de la fonction sont bien ordonn�es et mal ordonn�es.
   */
  @Test
  public void constructorSRealFunctionTest1() 
  {
    double[] values = { 3.0, 6.0, 9.0, 12.0, 15.0 };
    
    SRealFunction f_1 = new SRealFunction(values, 1.0, 5.0);
    
    try{
      SRealFunction f_2 = new SRealFunction(values, 5.0, 1.0);
      fail("Ce test est en �chec, car les bornes maximales et minimales de la fonction sont invers�es!");
      System.out.println(f_1);  // retirer warning !
      System.out.println(f_2);  // retirer warning !
    }catch(SConstructorException e){
      // Rien ... tout est beau.
    }
    
  }
       
  /**
   * Test de la m�thode <b>x</b> dans le domaine [0, 10] pour 11 �l�ments.
   */
  @Test
  public void xTest1()
  {
    try{
      
      double[] x = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
      
      SRealFunction calculated = SRealFunction.x(11, 0.0, 10.0);
      SRealFunction expected = new SRealFunction(x, 0.0, 10.0);
      
      Assert.assertEquals(expected, calculated);
            
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void xTest1()");
    }
  }
  
  /**
   * Test de la m�thode <b>x</b> dans le domaine [-5, 5] pour 11 �l�ments.
   */
  @Test
  public void xTest2()
  {
    try{
      
      double[] x = { -5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
      
      SRealFunction calculated = SRealFunction.x(11, -5.0, 5.0);
      SRealFunction expected = new SRealFunction(x, -5.0, 5.0);
      
      Assert.assertEquals(expected, calculated);
            
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void xTest2()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>x</b> dans le domaine [1, 2] pour 5 �l�ments.
   */
  @Test
  public void xTest3()
  {
    try{
      
      double[] x = { 1.0, 1.25, 1.50, 1.75, 2.0 };
      
      SRealFunction calculated = SRealFunction.x(5, 1.0, 2.0);
      SRealFunction expected = new SRealFunction(x, 1.0, 2.0);
      
      Assert.assertEquals(expected, calculated);
            
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void xTest3()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>add</b> dans le cas o� les domaines des deux fonctions n'ont pas le m�me nombre d'�l�ments discret.
   * Une exception de type <b>SInvalidFunctionException</b> sera alors lanc�e.
   */
  @Test
  public void addTest1a()
  {
    try{
      
      SRealFunction f1 = SRealFunction.x(3000, 0.0, 10.0);
      SRealFunction f2 = SRealFunction.x(2000, 0.0, 10.0);
      
      try{
        f1.add(f2);
        fail("Ce test est un �chec, car les deux fonctions n'ont pas le m�me nombre d'�l�ments discrets.");
      }catch(SInvalidFunctionException e){
        // c'est un succ�s !
      }
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void addTest1a()");
    }
  }
  
  /**
   * Test de la m�thode <b>add</b> dans le cas o� les domaines des deux fonctions ne sont pas identique.
   * Une exception de type <b>SInvalidFunctionException</b> sera alors lanc�e.
   */
  @Test
  public void addTest1b()
  {
    try{
      
      SRealFunction f1 = SRealFunction.x(3000, 0.0, 10.0);
      SRealFunction f2 = SRealFunction.x(3000, 0.0, 5.0);
      
      try{
        f1.add(f2);
        fail("Ce test est un �chec, car les deux fonctions n'ont pas le m�me domaine.");
      }catch(SInvalidFunctionException e){
        // c'est un succ�s !
      }
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void addTest1b()");
    }
  }
  
  /**
   * Test de la m�thode <b>add</b> dans le cas de l'addition x + x = 2x.
   */
  @Test
  public void addTest2()
  {
    try{
      
      SRealFunction x = SRealFunction.x(3000, 0.0, 10.0);
      
      SRealFunction calculated = x.add(x);
      
      double[] x_plus_x = new double[3000];
         
      double dx = SDiscreteFunction.dx(x_plus_x, 0.0 , 10.0);
      
      for(int i = 0; i < x_plus_x.length; i++)
        x_plus_x[i] = 2*(i*dx);
      
      SRealFunction expected = new SRealFunction(x_plus_x, 0.0, 10.0);
      
      Assert.assertEquals(expected, calculated);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void addTest2()");
    }
  }
  
  /**
   * Test de la m�thode <b>pow</b> dans le cas de la fonction (3x)^2 = 9x^2.
   */
  @Test
  public void powTest1()
  {
    try{
      
      SRealFunction x = SRealFunction.x(30, 0.0, 10.0);
      
      SRealFunction f = x.multiply(3.0);
      
      SRealFunction calculated = f.pow(2.0);
      
      SRealFunction expected =f.multiply(f);
      
      Assert.assertEquals(expected, calculated);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void powTest1()");
    }
  }
  
  /**
   * Test de la m�thode <b>pow</b> dans le cas de la fonction 1 / x = x^-1.
   */
  @Test
  public void powTest2()
  {
    try{
      
      SRealFunction one = SRealFunction.one(30, 0.0, 10.0);
      SRealFunction x = SRealFunction.x(30, 0.0, 10.0);
      
      SRealFunction calculated = x.pow(-1.0);
      
      SRealFunction expected = one.divide(x);
      
      Assert.assertEquals(expected, calculated);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SRealFunctionTest ---> Test non effectu� : public void powTest2()");
    }
  }
  
}//fin de la classe SRealFunctionTest
