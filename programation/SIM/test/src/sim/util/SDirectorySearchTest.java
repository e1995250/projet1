/**
 * 
 */
package sim.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit test permettant de valider les fonctionnalit�s de la classe <b>SDirectorySearch</b>.
 * 
 * @author Simon V�zina
 * @since 2018-02-22
 * @version 2018-02-23
 */
public class SDirectorySearchTest {

  /**
   * Test permettant de v�rifier la m�thode <b>isDirectoryFound</b> dans un sc�nario o� rien n'est trouv�.
   */
  @Test
  public void isDirectoryFoundTest1()
  {
    SDirectorySearch search = new SDirectorySearch("test_repertoire_qui_n_est_pas_trouvable");
    
    Assert.assertEquals(false, search.isDirectoryFound());
  }

  /**
   * Test permettant de v�rifier la m�thode <b>isDirectoryFound</b> dans un sc�nario o� le r�pertoire est trouv�.
   */
  @Test
  public void isDirectoryFoundTest2()
  {
    SDirectorySearch search = new SDirectorySearch("test_repertoire_qui_est_pas_trouvable_1_fois");
    
    Assert.assertEquals(true, search.isDirectoryFound());
  }
  
  /**
   * Test permettant de v�rifier la m�thode <b>isDirectoryFound</b> dans un sc�nario o� le r�pertoire n'est pas trouv� dont la recherche d�bute dans un sous-r�pertoire.
   */
  @Test
  public void isDirectoryFoundTest3()
  {
    String subdirectory = "test" + SStringUtil.FILE_SEPARATOR_CARACTER + "test_data";
    
    SDirectorySearch search = new SDirectorySearch(subdirectory,"test_repertoire_qui_n'est_pas_trouvable");
    
    Assert.assertEquals(false, search.isDirectoryFound());
  }
  
  /**
   * Test permettant de v�rifier la m�thode <b>isDirectoryFound</b> dans un sc�nario o� le r�pertoire est trouv� dont la recherche d�bute dans un sous-r�pertoire.
   */
  @Test
  public void isDirectoryFoundTest4()
  {
    String subdirectory = "test" + SStringUtil.FILE_SEPARATOR_CARACTER + "test_data";
    
    SDirectorySearch search = new SDirectorySearch(subdirectory,"test_repertoire_qui_est_pas_trouvable_1_fois");
    
    Assert.assertEquals(true, search.isDirectoryFound());
  }
  
  /**
   * Test permettant de v�rifier la m�thode <b>isManyDirectoryFound</b> dans un sc�nario o� le r�pertoire est trouv� seulement une fois.
   */
  @Test
  public void isManyDirectoryFoundTest1()
  {
    SDirectorySearch search = new SDirectorySearch("test_repertoire_qui_est_pas_trouvable_1_fois");
    
    Assert.assertEquals(false, search.isManyDirectoryFound());
  }
  
  /**
   * Test permettant de v�rifier la m�thode <b>isManyDirectoryFound</b> dans un sc�nario o� le r�pertoire est trouv� deux fois.
   */
  @Test
  public void isManyDirectoryFoundTest2()
  {
    SDirectorySearch search = new SDirectorySearch("test_repertoire_qui_est_pas_trouvable_2_fois");
    
    Assert.assertEquals(true, search.isManyDirectoryFound());
  }
  
  /**
   * Test permettant de v�rifier la m�thode <b>isManyDirectoryFound</b> dans un sc�nario o� le r�pertoire est trouv� une fois 
   * �tant donn�e que la recherche d�but dans un sous-r�pertoire o� il n'y aura qu'une seule pr�sence du r�pertoire en recherche.
   */
  @Test
  public void isManyDirectoryFoundTest3()
  {
    String subdirectory = "test" + SStringUtil.FILE_SEPARATOR_CARACTER + "test_data" + SStringUtil.FILE_SEPARATOR_CARACTER + "test_repertoire_qui_est_pas_trouvable_1_fois";
    
    SDirectorySearch search = new SDirectorySearch(subdirectory, "test_repertoire_qui_est_pas_trouvable_2_fois");
    
    Assert.assertEquals(false, search.isManyDirectoryFound());
  }
  
}// fin de la classe SDirectorySearchTest
