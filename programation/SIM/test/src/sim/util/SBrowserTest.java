package sim.util;

import static org.junit.Assert.fail;

import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit test permettant de valider les fonctionnalit�s de la classe <b>SFileSearch</b>.
 * 
 * @author Simon V�zina
 * @since 2018-03-13
 * @version 2018-03-14
 */
public class SBrowserTest {

  @Test
  public void goToTest1() throws FileNotFoundException, SManyFilesFoundException 
  {
    SBrowser browser = new SBrowser();
    browser.enter("test");
    browser.enter("test_data");
    
    browser.goTo("test_repertoire_qui_est_pas_trouvable_1_fois");
    
    String expected_solution = System.getProperty("user.dir") + SStringUtil.FILE_SEPARATOR_CARACTER + "test" + SStringUtil.FILE_SEPARATOR_CARACTER + "test_data" + SStringUtil.FILE_SEPARATOR_CARACTER + "test_repertoire_qui_est_pas_trouvable_1_fois";
    
    String calculated_solution = browser.getPath();
    
    Assert.assertEquals(expected_solution, calculated_solution);
  }
  
  @Test
  public void goToTest2() throws FileNotFoundException, SManyFilesFoundException 
  {
    SBrowser browser = new SBrowser();
    browser.enter("test");
    browser.enter("test_data");
    
    try{
      
      browser.goTo("test_repertoire_qui_est_pas_trouvable_2_fois");
      
      fail("Ce test devrait �tre en �chec, car le r�pertoire 'test_repertoire_qui_est_pas_trouvable_2_fois' existe deux fois.");
      
    }catch(SManyFilesFoundException e){
      // C'est un succ�s !
    }
  }
  
  @Test
  public void enterTest1() throws FileNotFoundException, SManyFilesFoundException 
  {
    SBrowser browser = new SBrowser();
    browser.enter("test");
    browser.enter("test_data");
    
    String expected_solution = System.getProperty("user.dir") + SStringUtil.FILE_SEPARATOR_CARACTER + "test" + SStringUtil.FILE_SEPARATOR_CARACTER + "test_data";
    
    String calculated_solution = browser.getPath();
    
    Assert.assertEquals(expected_solution, calculated_solution);
  }

  @Test
  public void backTest1() throws FileNotFoundException, SManyFilesFoundException 
  {
    SBrowser browser = new SBrowser();
    
    browser.enter("test");
    browser.enter("test_data");
    browser.back();
    
    String expected_solution = System.getProperty("user.dir") + SStringUtil.FILE_SEPARATOR_CARACTER + "test";
    
    String calculated_solution = browser.getPath();
    
    Assert.assertEquals(expected_solution, calculated_solution);
  }
    
  @Test
  public void findFileTest1() throws FileNotFoundException, SManyFilesFoundException
  {
    SBrowser browser = new SBrowser();
    browser.enter("test");
    browser.enter("test_data");   
    
    String expected_solution = "test_fichier_qui_est_trouvable_1_fois.txt";
    
    String calculated_solution = browser.findFile("test_fichier_qui_est_trouvable_1_fois.txt").getName();
    
    Assert.assertEquals(expected_solution, calculated_solution);
  }
  
  @Test
  public void findFileTest2() throws FileNotFoundException, SManyFilesFoundException
  {
    SBrowser browser = new SBrowser();
    browser.enter("test");
    browser.enter("test_data");
    
    try{
      
      browser.findFile("test_fichier_qui_est_trouvable_2_fois.txt").getName();
    
      fail("Ce test devrait �tre en �chec, car le fichier 'test_fichier_qui_est_trouvable_2_fois.txt' existe deux fois.");
      
    }catch(SManyFilesFoundException e){
      // C'est un succ�s !
    }
  }
  
}
