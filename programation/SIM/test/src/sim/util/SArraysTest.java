/**
 * 
 */
package sim.util;

import org.junit.Assert;
import org.junit.Test;

import sim.exception.SNoImplementationException;
import sim.math.SMath;

/**
 * JUnit test permettant de valider les fonctionnalit�s de la classe <b>SArrayUtil</b>.
 * 
 * @author Simon V�zina
 * @since 2018-01-23
 * @version 2018-12-06
 */
public class SArraysTest {

  /**
   * Test de la m�thode <b>add</b> o� l'on v�rifie qu'il y a eu allocation de m�moire pour retourner le r�sultat.
   */
  @Test
  public void addTest1a()
  {
    try{
      
      double[] A = {  };
      double[] B = {  };
      
      double[] calculated = SArrays.add(A, B);
      
      // V�rification de la nouvelle allocation de m�moire
      Assert.assertNotEquals(calculated, A);
      Assert.assertNotEquals(calculated, B);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void addTest1a()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>add</b> o� l'on v�rifie que les deux tableaux pass�s en param�tre dans la fonction n'ont pas �t� modifi�s.
   */
  @Test
  public void addTest1b()
  {
    try{
      
      double[] A = { 1.0, 2.0, 3.0 };
      double[] A_original = { 1.0, 2.0, 3.0 };
      
      double[] B = { 4.0, 5.0, 6.0 };
      double[] B_original = { 4.0, 5.0, 6.0 };
      
      //------------------
      // M�thode � tester.
      //------------------
      double[] calculated = SArrays.add(A, B);
      
      // V�rification de la nouvelle allocation de m�moire.
      Assert.assertNotEquals(calculated, A);
      Assert.assertNotEquals(calculated, B);
      
      // V�rifier que le contenu de A n'a pas �t� modifi�.
      Assert.assertArrayEquals(A_original, A, 0.000001);
      
      // V�rifier que le contenu de B n'a pas �t� modifi�.
      Assert.assertArrayEquals(B_original, B, 0.000001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void addTest1b()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>add</b> dans un cas simple o� la somme des deux tableaux donne z�ro pour l'ensemble des �l�ments.
   */
  @Test
  public void addTest2a()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
      double[] B = { 0.0, -1.0, -2.0, -3.0, -4.0, -5.0 };
      
      double[] calculateds = SArrays.add(A, B);
      double[] expecteds = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
      
      Assert.assertArrayEquals(expecteds, calculateds, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void addTest2a()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>add</b> dans un cas simple o� les deux tableaux sont identiques.
   */
  @Test
  public void addTest2b()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
            
      double[] calculateds = SArrays.add(A, A);
      double[] expecteds = { 0.0, 2.0, 4.0, 6.0, 8.0, 10.0 };
      
      Assert.assertArrayEquals(expecteds, calculateds, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void addTest2b()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>add</b> dans un cas quelconque.
   */
  @Test
  public void addTest3()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
      double[] B = { 5.0, 7.0, 9.0, 12.0, 16.0, 22.0 };
      
      double[] calculateds = SArrays.add(A, B);
      double[] expecteds = { 5.0, 8.0, 11.0, 15.0, 20.0, 27.0 };
      
      Assert.assertArrayEquals(expecteds, calculateds, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void addTest3()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>substract</b> o� l'on v�rifie qu'il y a eu allocation de m�moire pour retourner le r�sultat.
   */
  @Test
  public void substractTest1()
  {
    try{
      
      double[] A = {  };
      double[] B = {  };
      
      double[] calculated = SArrays.substract(A, B);
      
      // V�rification de la nouvelle allocation de m�moire
      Assert.assertNotEquals(calculated, A);
      Assert.assertNotEquals(calculated, B);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void substractTest1()");
    }
    
  }
  
  
  /**
   * Test de la m�thode <b>substract</b> dans un cas simple.
   */
  @Test
  public void substractTest2()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
      double[] B = { 0.0, -1.0, -2.0, -3.0, -4.0, -5.0 };
      
      double[] calculateds = SArrays.substract(A, B);
      double[] expecteds = { 0.0, 2.0, 4.0, 6.0, 8.0, 10.0 };
      
      Assert.assertArrayEquals(expecteds, calculateds, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void substractTest2()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>multiply</b> pour une <b>scalaire</b> o� l'on v�rifie qu'il y a eu allocation de m�moire pour retourner le r�sultat.
   */
  @Test
  public void multiplyScalarTest1()
  {
    try{
      
      double[] A = {  };
            
      double[] calculated = SArrays.multiply(A, 5.6);
      
      // V�rification de la nouvelle allocation de m�moire
      Assert.assertNotEquals(calculated, A);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void multiplyScalarTest1()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>multiply</b> pour une <b>scalaire</b> avec un cas simple.
   */
  @Test
  public void multiplyScalarTest2()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
            
      double[] calculateds = SArrays.multiply(A, 3.0);
      double[] expecteds = { 0.0, 3.0, 6.0, 9.0, 12.0, 15.0 };
      
      Assert.assertArrayEquals(expecteds, calculateds, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void multiplyScalarTest2()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>multiply</b> pour des <b>Array</b> o� l'on v�rifie qu'il y a eu allocation de m�moire pour retourner le r�sultat.
   */
  @Test
  public void multiplyArrayTest1()
  {
    try{
      
      double[] A = {  };
      double[] B = {  };
      
      double[] calculated = SArrays.multiply(A, B);
      
      // V�rification de la nouvelle allocation de m�moire
      Assert.assertNotEquals(calculated, A);
      Assert.assertNotEquals(calculated, B);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void multiplyArrayTest1()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>multiply</b> pour des <b>Array</b> dans un cas simple.
   */
  @Test
  public void multiplyArrayTest2()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
      double[] B = { 0.0, -1.0, -2.0, -3.0, -4.0, -5.0 };
      
      double[] calculateds = SArrays.multiply(A, B);
      double[] expecteds = { 0.0, -1.0, -4.0, -9.0, -16.0, -25.0 };
      
      Assert.assertArrayEquals(expecteds, calculateds, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void multiplyArrayTest2()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>divide</b> o� l'on v�rifie qu'il y a eu allocation de m�moire pour retourner le r�sultat.
   */
  @Test
  public void divideTest1()
  {
    try{
      
      double[] A = {  };
      double[] B = {  };
      
      double[] calculated = SArrays.divide(A, B);
      
      // V�rification de la nouvelle allocation de m�moire
      Assert.assertNotEquals(calculated, A);
      Assert.assertNotEquals(calculated, B);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void divideTest1()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>divide</b> dans un cas simple.
   */
  @Test
  public void divideTest2()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
      double[] B = { 6.0, 5.0, 4.0, 3.0, 2.0, 1.0 };
      
      double[] calculateds = SArrays.divide(A, B);
      double[] expecteds = { 0.0, 0.2, 0.5, 1.0, 2.0, 5.0 };
      
      Assert.assertArrayEquals(expecteds, calculateds, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void divideTest2()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>pow</b> o� l'on v�rifie qu'il y a eu allocation de m�moire pour retourner le r�sultat.
   */
  @Test
  public void powTest1()
  {
    try{
      
      double[] A = {  };
           
      double[] calculated = SArrays.pow(A, 4.6);
      
      // V�rification de la nouvelle allocation de m�moire
      Assert.assertNotEquals(calculated, A);
            
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void powTest1()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>pow</b> dans un cas simple.
   */
  @Test
  public void powTest2()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
            
      double[] calculateds = SArrays.pow(A, 3.0);
      double[] expecteds = { 0.0, 1.0, 8.0, 27.0, 64.0, 125.0 };
      
      Assert.assertArrayEquals(expecteds, calculateds, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void powTest2()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>dot</b> dans un cas simple.
   */
  @Test
  public void dotTest1()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
      double[] B = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
      
      double calculated = SArrays.dot(A, B);
      double expected = 15.0;
      
      Assert.assertEquals(expected, calculated, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void dotTest1()");
    }
    
  }
  
  /**
   * Test de la m�thode <b>dot</b> dans un cas quelconque.
   */
  @Test
  public void dotTest2()
  {
    try{
      
      double[] A = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 };
      double[] B = { 4.0, -2.0, 6.0, -8.0, 7.0, 2.0 };
      
      double calculated = SArrays.dot(A, B);
      double expected = 24.0;
      
      Assert.assertEquals(expected, calculated, 0.0001);
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("SArraysTest ---> Test non effectu� : public void dotTest2()");
    }
    
  }
  
  /**
   * JUnit Test de la m�thode findMax.
   */
  @Test
  public void findMaxTest()
  {
    int[] tab = { 45, 76, 23, -45, -123, 32, 67 };
    
    Assert.assertEquals(76, SArrays.findMax(tab));
  }
  
  /**
   * JUnit Test de la m�thode findMin.
   */
  @Test
  public void findMinTest()
  {
    int[] tab = { 45, 76, 23, -45, -123, 32, 67 };
    
    Assert.assertEquals(-123, SArrays.findMin(tab));
  }
  
  /**
   * JUnit Test de la m�thode intersectionSortedArray.
   */
  @Test
  public void intersectionSortedArrayTest1()
  {
   double[] tab1 = { 2.3, 4.5, 7.4, 9.4, 12.3 };
   double[] tab2 = { 2.6, 4.5, 7.2, 9.4, 14.3 };
   
   double[] calculated_solution = SArrays.intersectionSortedArray(tab1, tab2);
   double[] expected_solution = { 4.5, 9.4 };
   
   Assert.assertArrayEquals(expected_solution, calculated_solution, SMath.EPSILON);
  }

}// fin de la classe SArrayUtilTest
