/**
 * 
 */
package sim.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit test permettant de valider les fonctionnalit�s de la classe <b>SFileSearch</b>.
 * 
 * @author Simon V�zina
 * @since 2015-10-03
 * @version 2018-03-13
 */
public class SFileSearchTest {

  /**
   * Test permettant de v�rifier la m�thode isFileFound() dans un sc�nario o� rien n'est trouv�.
   */
  @Test
  public void test_isFileFound()
  {
    SFileSearch search = new SFileSearch("Fichier qui n'est pas trouvable.");
    
    Assert.assertEquals(false, search.isFileFound());
  }

  /**
   * Test permettant de v�rifier la m�thode isManyFileFound() dans un sc�nario o� rien n'est trouv�.
   */
  @Test
  public void test_isManyFileFound()
  {
    SFileSearch search = new SFileSearch("Fichier qui n'est pas trouvable.");
    
    Assert.assertEquals(false, search.isManyFileFound());
  }
  
}
