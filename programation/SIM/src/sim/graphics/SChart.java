/**
 * 
 */
package sim.graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import sim.exception.SConstructorException;
import sim.math.SRealFunction;
import sim.util.SArrays;

/**
 * La classe <b>SChart</b> repr�sente un graphique en nuage de point.
 * 
 * @author Simon V�zina
 * @since 2018-04-10
 * @version 2018-05-18
 */
public class SChart {

  private static final String DEFAULT_TITLE = "";
  
  private static final Color function_color = Color.BLACK;
  private static final Color axis_color = Color.RED;
  private static final Color graduation_color = Color.CYAN;
  private static final Color title_color = Color.BLUE;
  private static final Color legend_color = Color.BLUE;
    
  private final SRealFunction f;
  private final double y_min;
  private final double y_max;
  
  private final int height;
  
  private final String title;
  
  /**
   * ...
   * 
   * @param f
   * @param y_min
   * @param y_max
   * @param height
   * @throws SConstructorException
   */
  public SChart(SRealFunction f, double y_min, double y_max, int height) throws SConstructorException
  {
    this(f, y_min, y_max, height, DEFAULT_TITLE);
  }
  
  /**
   * ...
   * 
   * @param f
   * @param height
   * @param title
   * @throws SConstructorException
   */
  public SChart(SRealFunction f, int height, String title) throws SConstructorException
  {
    this(f, findBestMinY(f.copyValues()), findBestMaxY(f.copyValues()), height, title);
  }
  
  /**
   * ...
   * 
   * @param f
   * @param y_min
   * @param y_max
   * @param height
   * @param title
   * @throws SConstructorException
   */
  public SChart(SRealFunction f, double y_min, double y_max, int height, String title) throws SConstructorException
  {
    if(f == null)
      throw new SConstructorException("Erreur SChart 001 : La fonction est 'null'.");
    
    this.f = f;
    this.y_min = y_min;
    this.y_max = y_max;
    this.height = height;
    this.title = title;
  }
  
  /**
   * ...
   * 
   * @return
   */
  public BufferedImage buildBufferedImage()
  {
    int width = f.getNumberOfDiscreteElements();
    
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    SImageUtil.fillImageWhite(image);
    
    // Affichage de l'axe x et y aves ses graduations.
    double dx = evaluateDx();
    double dy = evaluateDy();
    
    // Dessiner les graduation.
    drawGraduationAxisX(image, dx);
    drawGraduationAxisY(image, dy);
    
    // Dessiner les axes.
    drawAxisX(image);
    drawAxisY(image);
    
    // �criture du titre.
    drawTitle(title, image);
       
    // �criture de la l�gende.
    drawLegend("Graduation x = " + dx + " , Graduation y = " + dy, image);
    
    // Dessiner la fonction.
    drawFunction(image);
               
    return image;
  }
  
  /**
   * M�thode pour �valuer la graduation des axes.
   * 
   * @param min Valeur minimale le long de l'axe.
   * @param max Valeur maximal le long de l'axe.
   * @return La graduation de l'axe.
   */
  private double evaluateAxisGraduation(double min, double max)
  {
    double delta = max - min;
    
    int log_value = (int) Math.round(Math.log10(delta));
    
    // Si n�gatif --> les divisions seront entre 0 et 1.
    if(log_value == 0)
      return 0.1;
    else
      if(log_value < 0)
        return 1 / Math.pow(10.0, Math.abs(log_value)+1);
      else
        return Math.pow(10.0, log_value-1);
  }
  
  /**
   * M�thode pour dessin la fonction.
   * 
   * @param image L'image sur laquelle la fonction sera dessin�e.
   */
  private void drawFunction(BufferedImage image)
  {
    int[] image_data = SArrays.mappingDoubleToInt(f.copyValues(), y_min, y_max, 0, height-1);
    
    // Affichage de la fonction pour les valeurs d�finies (il faut exclure les valeurs NaN).      
    for(int i = 0; i < image.getWidth(); i++)
      if(image_data[i] != (int) Double.NaN)
      {
        // �valuer la position verticale dans l'image.
        int j = (image.getHeight()-1) - image_data[i];
              
        // V�rifier que la position verticale est situ�e dans la hauteur de l'image.
        if(j >=0 && j < image.getHeight())
          image.setRGB(i, j, function_color.getRGB());
      }
  }
  
  /**
   * ...
   * 
   * @param image
   */
  private void drawAxisX(BufferedImage image)
  {
    double[] zero = { 0.0 };
    int[] mapping = SArrays.mappingDoubleToInt(zero, y_min, y_max, 0, image.getHeight()-1);
        
    // V�rifier si l'axe x est dans la zone d'affichage. Si non, la graduation �galement ne sera pas affichable.
    if(mapping[0] >= 0 && mapping[0] < image.getHeight())
    {
      // Il ne faut pas oublier l'inversion des donn�es en y.
      int axis_j = (image.getHeight()-1) - mapping[0];
      
      // Dessiner la ligne repr�sentant l'axe x.
      for(int i = 0; i < image.getWidth(); i++)
        image.setRGB(i, axis_j, axis_color.getRGB());   
    }
  }
  
  /**
   * ...
   * 
   * @param image
   * @param zero_x
   * @param zero_y
   * @param dx
   */
  private void drawGraduationAxisX(BufferedImage image, double dx)
  {
    double x;
    
    // Dessiner les graduations sup�rieures � x = 0.
    x = 0.0;
    
    if( 0.0 < f.getMaxX())
      do{
        
        x = x + dx;
        
        double[] tab = { x };
        
        int[] mapping = SArrays.mappingDoubleToInt(tab, f.getMinX(), f.getMaxX(), 0, image.getWidth()-1);
        
        int i = mapping[0];
        
        if(i >= 0 && i < image.getWidth())
          for(int j = 0; j < image.getHeight(); j++)
            image.setRGB(i, j, graduation_color.getRGB());
        
      }while(x < f.getMaxX());
    
    // Dessiner les graduations inf�rieures � x = 0.
    x = 0.0;
    
    if( 0.0 > f.getMinX())
      do{
        
        x = x - dx;
        
        double[] tab = { x };
        
        int[] mapping = SArrays.mappingDoubleToInt(tab, f.getMinX(), f.getMaxX(), 0, image.getWidth()-1);
        
        int i = mapping[0];
        
        if(i >= 0 && i < image.getWidth())
          for(int j = 0; j < image.getHeight(); j++)
            image.setRGB(i, j, graduation_color.getRGB());
        
      }while(x > f.getMinX());
        
  }
  
  /**
   * ...
   * 
   * @param image
   */
  private void drawAxisY(BufferedImage image)
  {
    double[] zero = { 0.0 };
    int[] mapping = SArrays.mappingDoubleToInt(zero, f.getMinX(), f.getMaxX(), 0, image.getWidth()-1);
        
    // Affichage de l'axe y en x = 0.
    if(mapping[0] >= 0 && mapping[0] < image.getWidth())
    {
      int axis_i = mapping[0];
      
      // Dessiner la ligne repr�sentant l'axe y.
      for(int j = 0; j < image.getHeight(); j++)
        image.setRGB(axis_i, j, axis_color.getRGB()); 
    }
  }
  
  /**
   * ...
   * 
   * @param image
   * @param zero_x
   * @param zero_y
   * @param dy
   */
  private void drawGraduationAxisY(BufferedImage image, double dy)
  {
   
    double y;
    
    // Dessiner les graduations sup�rieures � y = 0.
    y = 0.0;
    
    if( 0.0 < y_max)
      do{
        
        y = y + dy;
        
        double[] tab = { y };
        
        int[] mapping = SArrays.mappingDoubleToInt(tab, y_min, y_max, 0, image.getHeight()-1);
        
        int j = mapping[0];
        
        if(j >= 0 && j < image.getHeight())
          for(int i = 0; i < image.getWidth(); i++)
            image.setRGB(i, (image.getHeight()-1) - j, graduation_color.getRGB());
        
      }while(y < y_max);
    
    // Dessiner les graduations inf�rieures � x = 0.
    y = 0.0;
    
    if( 0.0 > y_min)
      do{
        
        y = y - dy;
        
        double[] tab = { y };
        
        int[] mapping = SArrays.mappingDoubleToInt(tab, y_min, y_max, 0, image.getHeight()-1);
        
        int j = mapping[0];
        
        if(j >= 0 && j < image.getHeight())
          for(int i = 0; i < image.getWidth(); i++)
            image.setRGB(i, (image.getHeight()-1) - j, graduation_color.getRGB());
        
      }while(y > y_min);
       
  }
  
  /**
   * M�thode pour r�aliser l'�criture d'un titre au graphique.
   * 
   * @param title Le titre � afficher.
   * @param image Le BufferedImage.
   */
  private void drawTitle(String title, BufferedImage image)
  {
    Graphics2D g2 = image.createGraphics();
    
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    Font font = new Font("TimesRoman", Font.PLAIN, fontSize(image.getHeight()));
    g2.setFont(font);
    g2.setColor(title_color);

    // Positionnement horizontal du titre � 20% du c�t� gauche.
    int x = (int) (image.getWidth()*0.2);
    
    // Positionnement vertical du titre � 10% du haut.
    int y = (int) (image.getHeight()*0.1);
    
    g2.drawString(title, x, y);
  }
  
  /**
   * M�thode pour r�aliser l'�criture de la l�gende du graphique.
   *   
   * @param legend La l�gende � afficher.
   * @param image Le BufferedImage.
   */
  private void drawLegend(String legend, BufferedImage image)
  {
    Graphics2D g2 = image.createGraphics();
    
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    Font font = new Font("TimesRoman", Font.PLAIN, fontSize(image.getHeight()));
    g2.setFont(font);
    g2.setColor(legend_color);

    // Positionnement horizontal du titre � 20% du c�t� gauche.
    int x = (int) (image.getWidth()*0.2);
    
    // Positionnement vertical du titre � 20% du haut.
    int y = (int) (image.getHeight()*0.2);
    
    g2.drawString(legend, x, y);
  }
  
  /**
   * M�thode pour d�terminer la taille de la police de caract�re.
   * 
   * @param h La hauteur de l'image.
   * @return La dimension de la police de caract�re.
   */
  private int fontSize(int h)
  {
    // �valuer la taille des caract�res � 5% de la taille verticale.
    int size = (int) (h * 0.05);
    
    // Utiliser au maximum 20 pixels pour la taille verticale.
    if(size > 40)
      return 40;
    else
      return size;
  }
  
  /**
   * ...
   * 
   * @return
   */
  private double evaluateDx()
  {
    return evaluateAxisGraduation(f.getMinX(), f.getMaxX());
  }
  
  /**
   * ...
   * 
   * @return
   */
  private double evaluateDy()
  {
    return evaluateAxisGraduation(y_min, y_max);
  }
  
  /**
   * VERSION � AM�LIORER !!!
   * 
   * @param tab
   * @return
   */
  private static double findBestMaxY(double[] tab)
  {
    double max = Double.NEGATIVE_INFINITY;
    
    for(double v : tab)
    {
      if(!Double.isNaN(v))
        if(v > max)
          max = v;
    }
    
    return max;
  }
  
  /**
   * VERSION � AM�LIORER !!!
   * 
   * @param tab
   * @return
   */
  private static double findBestMinY(double[] tab)
  {
    double min = Double.POSITIVE_INFINITY;
    
    for(double v : tab)
    {
      if(!Double.isNaN(v))
        if(v < min)
          min = v;
    }
     
    return min;
  }
  
}
