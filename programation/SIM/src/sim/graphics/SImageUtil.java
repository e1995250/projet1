/**
 * 
 */
package sim.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import sim.util.SStringUtil;

/**
 * La classe <b>SImageUtil</b> est un regroupement de fonctionnalit� en lien avec les images.
 * 
 * @author Simon V�zina
 * @since 2018-04-02
 * @version 2018-04-02
 */
public class SImageUtil {

  /**
   * M�thode pour g�n�rer une image BMP � partir d'un BufferedImage.
   * 
   * @param file_name Le nom du fichier (sans l'extension).
   * @param buffer Le buffer d'image.
   * @throws IllegalArgumentException Si le nom du fichier ne poss�de pas l'extension 'bmp'.
   */
  public static void writeImageBMP(String file_name, BufferedImage buffer) throws IllegalArgumentException
  {
    if(!SStringUtil.extensionFileLowerCase(file_name).equals("bmp"))
      throw new IllegalArgumentException("Erreur SImageUtil 001 : Le nom du fichier '" + file_name + "' n'a pas une extension �gale � 'bmp'.");
    
    try {
      
      File file = new File(file_name);
      ImageIO.write(buffer, "BMP", file);
      
    }catch(IOException e){
        e.printStackTrace();
    }
  }
  
  /**
   * M�thode pour g�n�rer une image PNG � partir d'un BufferedImage.
   * 
   * @param file_name Le nom du fichier (sans l'extension).
   * @param buffer Le buffer d'image.
   * @throws IllegalArgumentException Si le nom du fichier ne poss�de pas l'extension 'bmp'.
   */
  public static void writeImagePNG(String file_name, BufferedImage buffer) throws IllegalArgumentException
  {
    if(!SStringUtil.extensionFileLowerCase(file_name).equals("png"))
      throw new IllegalArgumentException("Erreur SImageUtil 002 : Le nom du fichier '" + file_name + "' n'a pas une extension �gale � 'png'.");
    
    try {
      
      File file = new File(file_name);
      ImageIO.write(buffer, "PNG", file);
      
    }catch(IOException e){
        e.printStackTrace();
    }
  }
  
  /**
   * M�thode pour remplir de <b>blanc</b> un BufferedImage.
   * 
   * @param buffer Le buffer.
   */
  public static void fillImageWhite(BufferedImage buffer)
  {
    Graphics2D ig2 = buffer.createGraphics();

    ig2.setBackground(Color.WHITE);
    ig2.clearRect(0, 0, buffer.getWidth(), buffer.getHeight());
  }
    
}
