/**
 * 
 */
package sim.math;

import java.util.Arrays;

import sim.exception.SConstructorException;
import sim.exception.SNoImplementationException;
import sim.util.SArrays;
import sim.util.SStringUtil;

/**
 * La classe <b>SRealFunction</b> repr�sente une fonction math�matique � valeur r�elle.
 * 
 * @author Simon V�zina
 * @since 2017-05-26
 * @version 2018-05-25 (Version labo 1.0 : Les fonctions discr�tes)
 */
public class SRealFunction {

  /**
   * La variable <b>fx</b> repr�sente de la fonction discr�te f(x).
   * accessible par des valeur discr�te des abscisses (x). 
   */
  private final double[] fx;
  
  /**
   * La variable <b>x_min</b> repr�sente la borne minimale du domaine de la fonction. 
   * � cette coordonn�e en x, on attribue l'indexage [0] dans le tableau des <b>fx</b>.
   */
  private final double x_min;
  
  /**
   * La variable <b>x_max</b> repr�sente la borne maximale du domaine de la fonction. 
   * � cette coordonn�e en x, on attribue l'indexage [N-1] dans le tableau des <b>fx</b>.
   */
  private final double x_max;
  
  //----------------
  // CONSTRUCTEUR //
  //----------------
  
  /**
   * Constructeur d'une fonction r�elle.
   * 
   * @param f Les donn�es de la fonction.
   * @param x_min La borne minimale du domaine.
   * @param x_final La borne maximale du domaine.
   * @throws SConstructorException Si la construction de la fonction est invalide.
   */
  public SRealFunction(double[] values, double x_min, double x_max) throws SConstructorException
  {
    // S'assurer que la fonction poss�de au moins 2 �l�ments.
    if(values.length < 2)
      throw new SConstructorException("Erreur SRealFunction 001 : Le nombre d'�l�ment dans la fonction est inf�rieur � 2 ce qui n'est pas une fonction.");
    
    // V�rifier que le d�but du domaine est inf�rieur � la fin du domaine.
    if(x_max < x_min)
      throw new SConstructorException("Erreur SRealFunction 002 : Le domaine de la fonction d�but � " + x_min + " et se termine � " + x_max + " ce qui n'est pas valide.");
    
    this.fx = new double[values.length];
    SArrays.copy(values, fx);
        
    this.x_min = x_min;
    this.x_max = x_max;
  }

  //-----------
  // M�THODE //
  //-----------
  
  /**
   * M�thode pour effectuer une copie des donn�es de la fonction.
   * 
   * @return Une copie des donn�es de la fonction.
   */
  public double[] copyValues()
  {
    double[] copy = new double[fx.length];
    
    SArrays.copy(fx, copy);
    
    return copy;
  }
  
  /**
   * M�thode pour obtenir la valeur <u>minimale</u> du domaine de la fonction.
   * 
   * @return La valeur minimale du domaine.
   */
  public double getMinX()
  {
    return x_min;
  }
  
  /**
   * M�thode pour obtenir la valeur <u>maximale</u> du domaine de la fonction.
   * 
   * @return La valeur maximale du domaine.
   */
  public double getMaxX()
  {
    return x_max;
  }
  
  /**
   * M�thode pour obtenir le nombre d'�l�ments discrets de la fonction.
   * 
   * @return Le nombre d'�l�ments discret.
   */
  public int getNumberOfDiscreteElements()
  {
    return fx.length;
  }
  
  /**
   * M�thode pour obtenir la distance dx entre deux �l�ments discrets cons�cutifs du domaine de la fonction.  
   * 
   * @return La distance dx entre deux �l�ments cons�cutifs du domaine.
   */
  public double dx()
  {
    return SDiscreteFunction.dx(fx, x_min, x_max);
  }
  
  /**
   * M�thode pour obtenir la valeur de la fonction en une coordonn�e <i>x</i>.
   * 
   * @param x La coordonn�e o� la fonction est �valu�e.
   * @return La valeur de la fonction en coordonn�e <i>x</i>.
   * @throws IndexOutOfBoundsException Si la coordonn�e <i>x</i> n'est pas dans le domaine de la fonction.
   */
  public double f(double x) throws IndexOutOfBoundsException
  {
    return SDiscreteFunction.f(x, fx, x_min, x_max);
  }
  
  /**
   * M�thode pour v�rifier si deux fonctions poss�dent le m�me domaine.
   * 
   * @param f La fonction dont le domaine sera compar�.
   * @return <b>true</b> si les deux fonctions ont le m�me domaine et <b>false</b> sinon.
   */
  public boolean isSameDomain(SRealFunction f)
  {
    return this.x_min == f.x_min && this.x_max == f.x_max;
  }
  
  /**
   * M�thode pour v�rifier si deux fonction poss�dent ne m�me nombre d'�l�ments discrets.
   * 
   * @param f La fonction � comparer.
   * @return <b>true</b> si les deux fonctions ont le m�me nombre d'�l�ments discret et <b>false</b> sinon.
   */
  public boolean isSameNumberOfDiscreteElements(SRealFunction f)
  {
    return this.fx.length == f.fx.length;
  }
  
  /**
   * M�thode pour faire <u>l'addition d'une constante</u> � une fonction.
   * 
   * @param a La constante.
   * @return La fonction correspondant au r�sultat de l'op�ration math�matique.
   */
  public SRealFunction add(double a)
  {
    return new SRealFunction(SArrays.add(this.fx, a), this.x_min, this.x_max);
  }
  
  /**
   * M�thode permettant de r�aliser <u>l'addition de deux fonctions</u>.
   * 
   * @param f La fonction � ajouter.
   * @return Une fonction repr�sentant la somme des deux fonctions.
   * @throws SInvalidFunctionException Si le domaine des deux fonctions n'est pas identique.
   * @throws SInvalidFunctionException Si le nombre d'�l�ments discrets des deux fonctions n'est pas identique.
   */
  public SRealFunction add(SRealFunction f) throws SInvalidFunctionException
  {
    if(!isSameDomain(f))
      throw new SInvalidFunctionException("Erreur SRealFunction 003 : Le domaine de la fonction � ajouter est [" + f.x_min + ", " + f.x_max + "] et le domaine de la fonction courante est [" + this.x_min + ", " + this.x_max + "] ce qui n'est pas semblable.");
      
    if(!isSameNumberOfDiscreteElements(f))
      throw new SInvalidFunctionException("Erreur SRealFunction 004 : Le nombre d'�l�ment discret de la fonction � ajouter est N = " + f.getNumberOfDiscreteElements() + "et la fonction courante en poss�de N = " + this.getNumberOfDiscreteElements() + " ce qui n'est pas semblable.");
    
    
    
    
    
    return new SRealFunction(SArrays.add(this.fx,f.fx ),this.x_min,x_max);
    
    
  }
    
  /**
   * M�thode pour faire <u>la soustraction d'une constante</u> � une fonction.
   * 
   * @param a La constante.
   * @return La fonction correspondant au r�sultat de l'op�ration math�matique.
   */
  public SRealFunction substract(double a)
  {
    return new SRealFunction(SArrays.add(this.fx, -1*a), this.x_min, this.x_max);
  }
  
  /**
   * M�thode permettant de r�aliser <u>la soustraction de deux fonctions</u>.
   * 
   * @param f La fonction � soustraire.
   * @return Une fonction repr�sentant la diff�rence des deux fonctions.
   * @throws SInvalidFunctionException Si le domaine des deux fonctions n'est pas identique.
   * @throws SInvalidFunctionException Si le nombre d'�l�ments discrets des deux fonctions n'est pas identique.
   */
  public SRealFunction substract(SRealFunction f) throws SInvalidFunctionException
  {
    if(!isSameDomain(f))
      throw new SInvalidFunctionException("Erreur SRealFunction 005 : Le domaine de la fonction � soustraire est [" + f.x_min + ", " + f.x_max + "] et le domaine de la fonction courante est [" + this.x_min + ", " + this.x_max + "] ce qui n'est pas semblable.");
      
    if(!isSameNumberOfDiscreteElements(f))
      throw new SInvalidFunctionException("Erreur SRealFunction 006 : Le nombre d'�l�ment discret de la fonction � ajouter est N = " + f.getNumberOfDiscreteElements() + "et la fonction courante en poss�de N = " + this.getNumberOfDiscreteElements() + " ce qui n'est pas semblable.");
        
    return new SRealFunction(SArrays.substract(this.fx, f.fx), this.x_min, this.x_max);
  }
  
  /**
   * M�thode pour faire <u>la multiplication d'une fonction par une constante</u>.
   * 
   * @param a La constante.
   * @return La fonction correspondant au r�sultat de l'op�ration math�matique.
   */
  public SRealFunction multiply(double a)
  {
    return new SRealFunction(SArrays.multiply(this.fx, a), this.x_min, this.x_max);
  }
  
  /**
   * M�thode permettant de r�aliser <u>la multiplication de deux fonctions</u>.
   * 
   * @param f La fonction � multiplier.
   * @return Une fonction repr�sentant le produit des deux fonctions.
   * @throws SInvalidFunctionException Si le domaine des deux fonctions n'est pas identique.
   * @throws SInvalidFunctionException Si le nombre d'�l�ments discrets des deux fonctions n'est pas identique.
   */
  public SRealFunction multiply(SRealFunction f) throws SInvalidFunctionException
  {
    if(!isSameDomain(f))
      throw new SInvalidFunctionException("Erreur SRealFunction 005 : Le domaine de la fonction � soustraire est [" + f.x_min + ", " + f.x_max + "] et le domaine de la fonction courante est [" + this.x_min + ", " + this.x_max + "] ce qui n'est pas semblable.");
      
    if(!isSameNumberOfDiscreteElements(f))
      throw new SInvalidFunctionException("Erreur SRealFunction 006 : Le nombre d'�l�ment discret de la fonction � ajouter est N = " + f.getNumberOfDiscreteElements() + "et la fonction courante en poss�de N = " + this.getNumberOfDiscreteElements() + " ce qui n'est pas semblable.");
    
    return new SRealFunction(SArrays.multiply(this.fx, f.fx), this.x_min, this.x_max);
  }
  
  /**
   * M�thode permettant de r�aliser <u>la division de deux fonctions</u>.
   * 
   * @param f La fonction � diviser.
   * @return Une fonction repr�sentant le quotient des deux fonctions.
   * @throws SInvalidFunctionException Si le domaine des deux fonctions n'est pas identique.
   * @throws SInvalidFunctionException Si le nombre d'�l�ments discrets des deux fonctions n'est pas identique.
   */
  public SRealFunction divide(SRealFunction f) throws SInvalidFunctionException
  {
    if(!isSameDomain(f))
      throw new SInvalidFunctionException("Erreur SRealFunction 007 : Le domaine de la fonction � soustraire est [" + f.x_min + ", " + f.x_max + "] et le domaine de la fonction courante est [" + this.x_min + ", " + this.x_max + "] ce qui n'est pas semblable.");
      
    if(!isSameNumberOfDiscreteElements(f))
      throw new SInvalidFunctionException("Erreur SRealFunction 008 : Le nombre d'�l�ment discret de la fonction � ajouter est N = " + f.getNumberOfDiscreteElements() + "et la fonction courante en poss�de N = " + this.getNumberOfDiscreteElements() + " ce qui n'est pas semblable.");
    
    return new SRealFunction(SArrays.divide(this.fx, f.fx), this.x_min, this.x_max);
  }
  
  /**
   * M�thode pour mettre une fonction � une certaine <u>puissance</u>.
   * 
   * @param n La puissance.
   * @return La fonction correspondant au r�sultat de l'op�ration math�matique.
   */
  public SRealFunction pow(double n)
  {
    return new SRealFunction(SArrays.pow(this.fx, n), this.x_min, this.x_max);
  }
  
  /**
   * M�thode pour effectuer la <u>d�riv�e</u> d'une fonction.
   * 
   * @return La fonction correspondant au r�sultat de l'op�ration math�matique.
   */
  public SRealFunction derivate()
  {
    double[] f_prime = SDiscreteFunction.derivate(fx, x_min, x_max);
    
    return new SRealFunction(f_prime, x_min, x_max);
  }
  
  /**
   * M�thode pour effectuer <u>l'int�grale</u> d'une fonction.
   * 
   * @param C La condition initiale au d�but du domaine de la fonction.
   * @return La fonction correspondant au r�sultat de l'op�ration math�matique.
   */
  public SRealFunction integrate(double C)
  {
    double[] F = SDiscreteFunction.integrate(fx, C, x_min, x_max);
    
    return new SRealFunction(F, x_min, x_max);
  }
  
    
  /**
   * M�thode qui g�n�re la fonction f(x) = x.
   * 
   * @param nb Le nombre d'�l�ments discrets dans la fonction f(x) = x.
   * @param min_x Le d�but du domaine de la fonction f(x) = x.
   * @param max_x La fin du domaine de la fonction f(x) = x.
   * @return La fonction f(x) = x.
   * @throws SConstructorException Si les param�tres ne permettent pas la construction de la fonction.
   */
  public static SRealFunction x(int nb, double x_min, double x_max) throws SConstructorException
  {
	
	double [] tab = new double[nb];
	double dx = SDiscreteFunction.dx(tab, x_min, x_max);
	
	for(int i =0;i<nb;i++) {
		
		tab[i]=x_min+i*dx;
		
		
		
	}
	
	return new SRealFunction(tab,x_min,x_max);
	
  }
  
  /**
   * M�thode qui g�n�re la fonction f(x) = 1.
   * 
   * @param nb Le nombre d'�l�ments discrets dans la fonction f(x) = 1.
   * @param min_x Le d�but du domaine de la fonction f(x) = 1.
   * @param max_x La fin du domaine de la fonction f(x) = 1.
   * @return La fonction f(x) = 1.
   * @throws SConstructorException Si les param�tres ne permettent pas la construction de la fonction.
   */
  public static SRealFunction one(int nb, double x_min, double x_max) throws SConstructorException
  {
    double[] f = new double[nb];
    
    for(int i = 0; i < f.length; i++)
      f[i] = 1;
    
    return new SRealFunction(f, x_min, x_max);  
  }
  
  
  /**
   * M�thode qui g�n�re <u>la fonction exponentielle</u> de la fonction f.
   * 
   * @param f La fonction qui sera en exponentielle.
   * @return La fonction g = exp(f).
   */
  public static SRealFunction exp(SRealFunction f)
  {
    return new SRealFunction(SArrays.exp(f.fx), f.getMinX(), f.getMaxX());
  }
   
  /**
   * M�thode qui g�n�re <u>la fonction sinus</u> de la fonction f.
   * 
   * @param f La fonction qui sera en argument dans la fonction sinus.
   * @return La fonction g = sin(f).
   */
  public static SRealFunction sin(SRealFunction f)
  {
    return new SRealFunction(SArrays.sin(f.fx), f.getMinX(), f.getMaxX());
  }
  
  /**
   * M�thode qui g�n�re <u>la fonction cosinus</u> de la fonction f.
   * 
   * @param f La fonction qui sera en argument dans la fonction cosinus.
   * @return La fonction g = cos(f).
   */
  public static SRealFunction cos(SRealFunction f)
  {
    return new SRealFunction(SArrays.cos(f.fx), f.getMinX(), f.getMaxX());
  }
  
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + Arrays.hashCode(fx);
    long temp;
    temp = Double.doubleToLongBits(x_max);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(x_min);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) 
  {
    if (this == obj)
      return true;
    
    if (obj == null)
      return false;
    
    if (getClass() != obj.getClass())
      return false;
    
    SRealFunction other = (SRealFunction) obj;
    
    // Comparaison des donn�es de la fonction avec la pr�cision EPSILON.
    if(!SArrays.nearlyEquals(fx, other.fx, SMath.EPSILON))
      return false;
    
    if (Double.doubleToLongBits(x_max) != Double.doubleToLongBits(other.x_max))
      return false;
    if (Double.doubleToLongBits(x_min) != Double.doubleToLongBits(other.x_min))
      return false;
    
    return true;
  }

  @Override
  public String toString()
  {
    return "SRealFunction [Nb = " + fx.length + ", x_min =" + x_min + ", x_max =" + x_max + "]" + SStringUtil.END_LINE_CARACTER + "Array = " + Arrays.toString(fx);
  }
  
}//fin de la classe SRealFunction
