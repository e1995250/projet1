/**
 * 
 */
package sim.math;

import sim.exception.SNoImplementationException;

/**
 * La classe <b>SDiscreteFunction</b> est une classe utilitaire permettant d'effectuer des op�rations math�matiques sur des fonctions discr�tes.
 * 
 * @author Simon V�zina
 * @since 2018-03-29
 * @version 2018-05-25 (Version labo 1.0 : Les fonctions discr�tes)
 */
public class SDiscreteFunction {

  /**
   * M�thode pour obtenir l'intervalle dx entre deux �l�ments cons�cutif de la fonction discr�te f(x).
   * 
   * @param fx Le tableau des valeurs de la fonction f(x).
   * @param x_min La valeur minimale du domaine.
   * @param x_max La valeur maximale du domaine.
   * @return La distance dx entre deux �l�ments discret du domaine de la fonction f(x).
   * @throws IllegalArgumentException Si le tableau de valeurs est de taille inf�rieure � 2.
   */
  public static double dx(double[] fx, double x_min, double x_max) throws IllegalArgumentException
  {
    if(fx.length < 2)
      throw new IllegalArgumentException("Erreur SDiscreteFunction 001 : La fonction fx poss�de moins de 2 �l�ments ce qui ne permet pas de d�finir un param�tre dx.");
    
    double dx= (x_max-x_min)/(fx.length-1);
	return dx;
  }
  
  /**
   * M�thode pour obtenir l'indice i associ� � une coordonn�e x d'une fonction discr�te f(x).
   * Lorsque x = x_min, i = 0. Lorsque x = x_max, i = length-1.
   * 
   * @param x La valeur x � convertir.
   * @param fx Le tableau repr�sentant la fonction.
   * @param x_min Le d�but du domaine.
   * @param x_max La fin du domaine.
   * @return L'indice dans le tableau de la fonction.
   */
  public static int xToIndex(double x, double[] fx, double x_min, double x_max) 
  {
    return (int)Math.round((x - x_min) / dx(fx, x_min, x_max));   
  }
  
  /**
   * M�thode pour obtenir une coordonn�e x associ� � un indice i d'une fonction discr�te f(x).
   * Lorsque i = 0, x = x_min. Lorsque i = length-1, x = x_max.
   * 
   * @param i L'indice dans le tableau de la fonction � convertir.
   * @param fx Le tableau repr�sentant la fonction.
   * @param x_min Le d�but du domaine.
   * @param x_max La fin du domaine.
   * @return La coordonn�e x.
   * @throws IndexOutOfBoundsException Si l'indice i n'est pas accessible dans le tableau.
   */
  public static double indexToX(int i, double[] fx, double x_min, double x_max) throws IndexOutOfBoundsException
  {
    return x_min + i*dx(fx, x_min, x_max);
  }
  
  /**
   * M�thode pour �valuer une fonction discr�te f(x) � une coordonn�e x.
   * 
   * @param x La coordonn�e x.
   * @param fx Le tableau repr�sentant la fonction.
   * @param x_min Le d�but du domaine.
   * @param x_max La fin du domaine.
   * @return La valeur de la fonction f(x) en coordonn�e x.
   * @throws IndexOutOfBoundsException
   */
  public static double f(double x, double[] fx, double x_min, double x_max) throws IndexOutOfBoundsException
  {
    return fx[xToIndex(x, fx, x_min, x_max)];
  }
  
  /**
   * M�thode pour �valuer la d�riv�e f'(x) d'une fonction discr�te f(x) � une coordonn�e x.
   * 
   * @param x La coordonn�e x.
   * @param fx Le tableau repr�sentant la fonction.
   * @param x_min Le d�but du domaine.
   * @param x_max La fin du domaine.
   * @return La d�riv�e de la fonction f(x) en coordonn�e x.
   * @throws IndexOutOfBoundsException
   */
  public static double dfdx(double x, double[] fx, double x_min, double x_max) throws IndexOutOfBoundsException
  {
    return dfdx(xToIndex(x, fx, x_min, x_max), fx, dx(fx, x_min, x_max));
  }
  
  /**
   * M�thode pour �valuer la d�riv�e f'(x) d'une fonction discr�te f(x) � un indice i.
   * 
   * @param i L'indice dans le tableau.
   * @param fx Le tableau repr�sentant la fonction.
   * @param dx La distance entre deux �l�ments cons�cutifs du tableau.
   * @return La d�riv�e de la fonction en indice i.
   * @throws IndexOutOfBoundsException Si l'indice i n'est pas accessible.
   */
  public static double dfdx(int i, double[] fx, double dx) throws IndexOutOfBoundsException
  {
    int a=fx.length;
	  if(i==0) {
	  
		 return rightDerivate(i,fx,dx);
	  }
	  if(fx.length-1==i) {
		  
		  return leftDerivate(i,fx,dx);
	  }
	  if(i!=0||fx.length-1!=i){
		 return middleDerivate(i,fx,dx);
	  
	  }
	return a;
	  
	  
  }
  
  /**
   * M�thode qui �value la d�riv�e f'(x) d'une fonction discr�te f(x) � un indice i selon la m�thode de la <u>d�riv�e � gauche</u>.
   * 
   * @param i L'indice dans le tableau.
   * @param fx Le tableau repr�sentant la fonction.
   * @param dx La distance entre deux �l�ments cons�cutifs du tableau.
   * @return La d�riv�e de la fonction en indice i.
   * @throws IndexOutOfBoundsException Si l'indice i n'est pas accessible.
   */
  public static double leftDerivate(int i, double[] fx, double dx) throws IndexOutOfBoundsException
  {
	  double res ;
  	res= (fx[i]-fx[i-1])/(dx);	
  return res;
  }
  
  /**
   * M�thode qui �value la d�riv�e f'(x) d'une fonction discr�te f(x) � un indice i selon la m�thode de la <u>d�riv�e � droite</u>.
   * 
   * @param i L'indice dans le tableau.
   * @param fx Le tableau repr�sentant la fonction.
   * @param dx La distance entre deux �l�ments cons�cutifs du tableau.
   * @return La d�riv�e de la fonction en indice i.
   * @throws IndexOutOfBoundsException Si l'indice i n'est pas accessible.
   */
  public static double rightDerivate(int i, double[] fx, double dx) throws IndexOutOfBoundsException
  {
    
	  double res ;
	    	res= (fx[i+1]-fx[i])/(dx);	
	    return res;
	  
  }
  
  /**
   * M�thode qui �value la d�riv�e f'(x) d'une fonction discr�te f(x) � un indice i selon la m�thode de la <u>d�riv�e au centre</u>.
   * 
   * @param i L'indice dans le tableau.
   * @param fx Le tableau repr�sentant la fonction.
   * @param dx La distance entre deux �l�ments cons�cutifs du tableau.
   * @return La d�riv�e de la fonction en indice i.
   * @throws IndexOutOfBoundsException Si l'indice i n'est pas accessible.
   */
  public static  double middleDerivate(int i, double[] fx, double dx) throws IndexOutOfBoundsException
  {
	  double res ;
  	res= (fx[i+1]-fx[i-1])/(2*dx);	
  return res;
  }
  
  /**
   * M�thode qui �value la d�riv�e f'(x) d'une fonction discr�te f(x) � un indice i selon la m�thode de la <u>d�riv�e � cinq points</u>.
   * 
   * R�f�rence : https://en.wikipedia.org/wiki/Five-point_stencil
   * 
   * @param i L'indice dans le tableau.
   * @param fx Le tableau repr�sentant la fonction.
   * @param dx La distance entre deux �l�ments cons�cutifs du tableau.
   * @return La d�riv�e de la fonction en indice i.
   * @throws IndexOutOfBoundsException Si l'indice i n'est pas accessible.
   */
  public static double fivePointsDerivate(int i, double[] fx, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * M�thode qui �value la d�riv�e discr�te f'(x) d'une fonction f(x).
   * 
   * @param fx Le tableau repr�sentant la fonction.
   * @param x_min Le d�but du domaine.
   * @param x_max La fin du domaine.
   * @return Un tableau correspondant � la d�riv�e de chaque �l�ment du tableau de la fonction pr�c�dente.
   */
  public static double[] derivate(double[] fx, double x_min, double x_max)
  {
	  
	  double dx =dx(fx,x_min,x_max);
	  
	  double[]res = new double[fx.length];
	    for(int i=0;i<fx.length;i++) {
	    	res[i]= dfdx(i,fx,dx);
	    	
	    }
	    
	    return res;
	  
  }
  
  /**
   * M�thode pour �valuer la primitive F(x) d'une fonction discr�te f(x) � une coordonn�e x.
   * 
   * @param x
   * @param fx
   * @param C La constante d'int�gration de la fonction F(x) qui est �valu�e � la coordonn�e x_min.
   * @param x_min
   * @param x_max
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double Fx(double x, double[] fx, double C, double x_min, double x_max) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e."); 
  }
  
  /**
   * M�thode pour �valuer la primitive F(x) d'une fonction discr�te f(x) entre un indice i et j.
   * 
   * @param i
   * @param j
   * @param fx
   * @param C
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double Fx(int i, int j, double[] fx, double C, double dx) throws IndexOutOfBoundsException, IllegalArgumentException
  {
    if(i > j)
      throw new IllegalArgumentException("Erreur SDiscreteFunction 005 : L'indice i = " + i + " est plus grand que l'argument j = " + j + ".");
   
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
    
  /**
   * ...
   * 
   * @param i
   * @param j
   * @param fx
   * @param C
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double leftIntegrate(int i, int j, double[] fx, double C, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * ...
   * 
   * @param i
   * @param j
   * @param fx
   * @param C
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double rightIntegrate(int i, int j, double[] fx, double C, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * ...
   * 
   * @param i
   * @param j
   * @param fx
   * @param C
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double middleIntegrate(int i, int j, double[] fx, double C, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * M�thode pour �valuer l'int�grale F(x) de la fonction f'(x) � l'indice i.
   *  
   * @param i
   * @param f
   * @param F Les valeurs de la fonction F(x) d�j� �valu�es qui serviront de conditions initiales (constante C) dans l'�valuation de la fonction pour d'autre indice i.
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double Fx(int i, double[] f, double[] F, double dx) throws IndexOutOfBoundsException 
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * ...
   * 
   * @param i
   * @param f
   * @param F
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double leftRuleIntegrate(int i, double[] f, double[] F, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * 
   * @param i
   * @param f
   * @param F
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double rightRuleIntegrate(int i, double[] f, double[] F, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * 
   * @param i
   * @param f
   * @param F
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double middleRuleIntegrate(int i, double[] f, double[] F, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * M�thode qui effectue le calcul de l'aire sous la courbe d'une fonction dans l'intervalle [i-2, i]
   * selon la m�thode de Simpson 1/3.
   * 
   * R�f�rence : https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Simpson
   * 
   * @param i
   * @param f
   * @param F
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double simpsonRuleIntegrate(int i, double[] f, double[] F, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * M�thode qui effectue le calcul de l'aire sous la courbe d'une fonction dans l'intervalle [i-3, i]
   * selon la m�thode de Simpson 3/8.
   * 
   * R�f�rence : http://mathworld.wolfram.com/Simpsons38Rule.html
   * 
   * @param i
   * @param f
   * @param F
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double simpsonAdvRuleIntegrate(int i, double[] f, double[] F, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * M�thode qui effectue le calcul de l'aire sous la courbe d'une fonction dans l'intervalle [i-4, i]
   * selon la m�thode de Boole.
   * 
   * R�f�rence : https://en.wikipedia.org/wiki/Boole%27s_rule
   * 
   * @param i
   * @param f
   * @param F
   * @param dx
   * @return
   * @throws IndexOutOfBoundsException
   */
  public static double booleRuleIntegrate(int i, double[] f, double[] F, double dx) throws IndexOutOfBoundsException
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * M�thode pour �valuer la prmitive F(x) d'une fonction discr�te f(x).
   * 
   * @param f
   * @param C
   * @param x_min
   * @param x_max
   * @return
   */
  public static double[] integrate(double[] f, double C, double x_min, double x_max)
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
}
