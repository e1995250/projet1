/**
 * 
 */
package sim.application;

import sim.exception.SConstructorException;
import sim.exception.SNoImplementationException;
import sim.graphics.SChart;
import sim.graphics.SImageUtil;
import sim.math.SRealFunction;
import sim.util.SLog;

/**
 * L'application <b>SIMFunction</b> repr�sente un espace pour utiliser des fonctions.
 * 
 * @author Simon V�zina
 * @since 2017-05-27
 * @version 2020-01-23 (Version labo : Les fonctions discr�tes)
 */
public class SIMFunction {

  /**
   * @param args
   */
  public static void main(String[] args) 
  {
    etape_1_1();
    etape_1_4();
    etape_2_3();
    etape_2_5();
    etape_3_4();
    etape_3_5();   
  }
    
  /**
   * M�thode correspondant � l'�tape 1.1 du laboratoire : Les fonctions discr�tes.
   * 
   * Cette m�thode r�alise l'affichage de la fonction f(x) = 1 dans un fichier image PNG.
   */
  private static void etape_1_1()
  {
    // D�finition du domaine de la fonctions.
    double x_min = -1.0;
    double x_max = 8.0;
    
    // D�finition du nombre d'�l�ments discret de la fonction discr�te.
    int nb = 2000;  
    
    // Construction de la foncion f(x) = 1 avec nb �l�ments discret sur le domaine [-1, 8].
    SRealFunction f = SRealFunction.one(nb, x_min, x_max);
    
    // Construction d'un graphique repr�sentant la fonction f(x).
    int height = 500;
    double y_min = -1.0;
    double y_max = 2.0;
    String title = "Graphique : f(x) = 1";
    SChart chart = new SChart(f, y_min, y_max, height, title);
    
    // Affichage d'une image correspondant au grahpique de la fonction f(x).
    String file_name = "etape_1_1.png"; 
    
    SImageUtil.writeImagePNG(file_name, chart.buildBufferedImage());
    
    SLog.logWriteLine("Fin de l'ex�cution de l'�tape 1.1 .");
  }
  
  /**
   * M�thode correspondant � l'�tape 1.4 du laboratoire : Les fonctions discr�tes.
   * 
   * Cette m�thode r�alise l'affichage de la fonction f(x) = x dans un fichier image PNG.
   */
  private static void etape_1_4()
  {
    try{
      
      // D�finition du domaine de la fonctions.
      double x_min = -1.0;
      double x_max = 8.0;
      
      // D�finition du nombre d'�l�ments discret de la fonction discr�te.
      int nb = 2000;  
      
      // Construction de la foncion f(x) = x avec nb �l�ments discret sur le domaine [-1, 8].
      
      //*******************************************************************************
      // � REMARQUER : C'EST ICI QUE L'ON A CHANG� LA FONCTION f(x) = 1 POUR f(x) = x.
      //               
      // La ligne de 1.1 �tait : SRealFunction f = SRealFunction.one(nb, x_min, x_max);
      //*******************************************************************************
      SRealFunction f = SRealFunction.x(nb, x_min, x_max);
      
      // Construction d'un graphique repr�sentant la fonction f(x).
      int height = 500;
      double y_min = -1.0;
      double y_max = 10.0;
      String title = "Graphique : f(x) = x";
      SChart chart = new SChart(f, y_min, y_max, height, title);
      
      // Affichage d'une image correspondant au grahpique de la fonction f(x).
      String file_name = "etape_1_4.png"; 
      
      SImageUtil.writeImagePNG(file_name, chart.buildBufferedImage());   
      
      SLog.logWriteLine("Fin de l'ex�cution de l'�tape 1.4 .");
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("L'ex�cution de l'�tape 1.4 n'a pas �t� r�alis�e, car une m�thode n�cessaire au fonctionnement n'a pas �t� impl�ment�e.");
    }
  }
  
  /**
   * M�thode correspondant � l'�tape 2.3 du laboratoire : Les fonctions discr�tes.
   * 
   * Cette m�thode r�alise l'affichage de la fonction f(x) = 3x + 1 dans un fichier image PNG.
   */
  private static void etape_2_3()
  {
    try{
      
      // D�finition du domaine de la fonctions.
      double x_min = -1.0;
      double x_max = 8.0;
      
      // D�finition du nombre d'�l�ments discret de la fonction discr�te.
      int nb = 2000;  
      
      // La fonction qui est � construire.
      SRealFunction f = null;
      
      //****************************************************************************************
      // Effectuez vos op�rations math�matiques ICI afin de d�finir ad�quatement la fonction f.
      //****************************************************************************************
      
      SRealFunction x = SRealFunction.x(nb, x_min, x_max);
      
      
      
      f = x.add(x.add(x.add(1)));
      
     
     
      
      // Construction d'un graphique repr�sentant la fonction f(x).
      int height = 500;
      double y_min = -10.0;
      double y_max = 40.0;
      String title = "Graphique : f(x) = 3x + 1";
      SChart chart = new SChart(f, y_min, y_max, height, title);
      
      // Affichage d'une image correspondant au grahpique de la fonction f(x).
      String file_name = "etape_2_3.png"; 
      
      SImageUtil.writeImagePNG(file_name, chart.buildBufferedImage());    
      
      SLog.logWriteLine("Fin de l'ex�cution de l'�tape 2.3 .");
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("L'ex�cution de l'�tape 2.3 n'a pas �t� r�alis�e, car une m�thode n�cessaire au fonctionnement n'a pas �t� impl�ment�e.");
    }catch(SConstructorException e){
      SLog.logWriteLine("L'ex�cution de l'�tape 2.3 n'a pas �t� r�alis�e, car la fonction a afficher est 'null'.");
    }
    
    
    
    
  }
  
  /**
   * M�thode correspondant � l'�tape 2.5 du laboratoire : Les fonctions discr�tes.
   * 
   * Cette m�thode r�alise l'affichage de la fonction f(x) = 4x^2 -5x + 1/(x-2) dans un fichier image PNG.
   */
  private static void etape_2_5()
  {
    try{    
      
      // D�finition du domaine de la fonctions.
      double  x_min = -2.0;
      double x_max = 5.0;
      
      // D�finition du nombre d'�l�ments discret de la fonction discr�te.
      int nb = 2000;  
      
      // La fonction qui est � construire.
      SRealFunction f = null;
      
      //****************************************************************************************
      // Effectuez vos op�rations math�matiques ICI afin de d�finir ad�quatement la fonction f.
      //****************************************************************************************
      SRealFunction x = SRealFunction.x(nb, x_min, x_max);
      
      f= x.pow(2).multiply(4);
      f=f.substract(x.multiply(5));
      f=f.add(x.substract(2).pow(-1));
      
      // Construction d'un graphique repr�sentant la fonction f(x).
      int height = 500;
      double y_min = -10.0;
      double y_max = 40.0;
      String title = "Graphique : f(x) = 4x^2 -5x + 1/(x-2)";
      SChart chart = new SChart(f, y_min, y_max, height, title);
      
      // Affichage d'une image correspondant au grahpique de la fonction f(x).
      String file_name = "etape_2_5.png"; 
      
      SImageUtil.writeImagePNG(file_name, chart.buildBufferedImage());    
      
      SLog.logWriteLine("Fin de l'ex�cution de l'�tape 2.5 .");
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("L'ex�cution de l'�tape 2.5 n'a pas �t� r�alis�e, car une m�thode n�cessaire au fonctionnement n'a pas �t� impl�ment�e.");
    }catch(SConstructorException e){
      SLog.logWriteLine("L'ex�cution de l'�tape 2.5 n'a pas �t� r�alis�e, car la fonction a afficher est 'null'.");
    }
    
  }
  
  /**
   * M�thode correspondant � l'�tape 3.4 du laboratoire : Les fonctions discr�tes.
   * 
   * Cette m�thode r�alise l'affichage de la fonction f(x) = 3x^2 + 5x - 6 et la fonction g(x) = f'(x).
   */
  private static void etape_3_4()
  {
    try{
      
      // D�finition du domaine de la fonctions.
      double  x_min = -3.0;
      double x_max = 3.0;
      
      // D�finition du nombre d'�l�ments discret de la fonction discr�te.
      int nb = 2000;  
      
      // La fonction qui est � construire.
      SRealFunction f = null;
      SRealFunction g = null;
        
      //********************************************************************************************
      // Effectuez vos op�rations math�matiques ICI afin de d�finir ad�quatement la fonction f et g.
      //********************************************************************************************
      SRealFunction x = SRealFunction.x(nb, x_min, x_max);
      
      f= x.pow(2).multiply(3);
      f=f.add(x.multiply(5));
      f=f.substract(6);
      g=f.derivate();
      
      // Construction d'un graphique repr�sentant la fonction f(x).
      int height = 500;
      double y_min = -10.0;
      double y_max = 20.0;
      String title = "Graphique : f(x) = 3x^2 + 5x - 6";
      SChart chart = new SChart(f, y_min, y_max, height, title);
      
      // Affichage d'une image correspondant au grahpique de la fonction f(x).
      String file_name = "etape_3_4a.png"; 
      
      SImageUtil.writeImagePNG(file_name, chart.buildBufferedImage());   
      
      // Construction d'un graphique repr�sentant la fonction f'(x).
      title = "Graphique : g(x) = f '(x)";
      chart = new SChart(g, y_min, y_max, height, title);
      file_name = "etape_3_4b.png"; 
      
      SImageUtil.writeImagePNG(file_name, chart.buildBufferedImage()); 
      
      SLog.logWriteLine("Fin de l'ex�cution de l'�tape 3.4 .");
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("L'ex�cution de l'�tape 3.4 n'a pas �t� r�alis�e, car une m�thode n�cessaire au fonctionnement n'a pas �t� impl�ment�e.");
    }catch(SConstructorException e){
      SLog.logWriteLine("L'ex�cution de l'�tape 3.4 n'a pas �t� r�alis�e, car la fonction a afficher est 'null'.");
    }
  }
  
  
  /**
   * M�thode correspondant � l'�tape 3.5 du laboratoire : Les fonctions discr�tes.
   * 
   * Cette m�thode r�alise l'affichage de la loi de Planck ainsi que sa d�riv�e.
   */
  private static void etape_3_5()
  {
    try{
      
      // D�finition du domaine de la fonctions.
      double x_min = 0;
      double x_max = 5e-6;
      
      // D�finition du nombre d'�l�ments discret de la fonction discr�te.
      int nb = 4000;  
          
      // La fonction qui est � construire.
      SRealFunction f = null;
      SRealFunction g = null;
      
      //********************************************************************************************
      // Effectuez vos op�rations math�matiques ICI afin de d�finir ad�quatement la fonction f et g.
      //********************************************************************************************
      SRealFunction x = SRealFunction.x(nb, x_min, x_max);
      
      f=f.add(2*Math.PI*6.63*Math.pow(10,-34)*Math.pow((3*Math.pow(10, 8)),2));
      f=f.divide(x.pow(5));
      f=f.divide(x.multiply(Math.E).divide(x).exp((x.multiply(1.38*Math.pow(10, -23)*3000).pow(-1)).multiply(6.63*Math.pow(10,-34)*3*Math.pow(10, 8)).substract(1)));
      
      
      
      // Construction d'un graphique repr�sentant la fonction f(x).
      int height = 2000;
      double y_min = 0.0;
      double y_max = 4e12;
      String title = "Graphique : La loi de Planck";
      SChart chart = new SChart(f, y_min, y_max, height, title);
            
      // Affichage d'une image correspondant au grahpique de la fonction f(x).
      String file_name = "etape_3_5a.png"; 
      
      SImageUtil.writeImagePNG(file_name, chart.buildBufferedImage());   
      
      // Construction d'un graphique repr�sentant la fonction f'(x).
      title = "Graphique : La d�riv�e de la loi de Planck par rapport � la longueur d'onde";
      y_min = -0.5e19;
      y_max = 1e19;
      chart = new SChart(g, y_min, y_max, height, title);
      file_name = "etape_3_5b.png"; 
      
      SImageUtil.writeImagePNG(file_name, chart.buildBufferedImage()); 
      
      SLog.logWriteLine("Fin de l'ex�cution de l'�tape 3.5 .");
      
    }catch(SNoImplementationException e){
      SLog.logWriteLine("L'ex�cution de l'�tape 3.5 n'a pas �t� r�alis�e, car une m�thode n�cessaire au fonctionnement n'a pas �t� impl�ment�e.");
    }catch(SConstructorException e){
      SLog.logWriteLine("L'ex�cution de l'�tape 3.5 n'a pas �t� r�alis�e, car la fonction a afficher est 'null'.");
    }
  }
    
}
