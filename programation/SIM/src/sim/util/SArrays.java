package sim.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import sim.exception.SNoImplementationException;
import sim.exception.SRuntimeException;
import sim.math.SMath;

/**
 * La classe <b>SArrayutil</b> permet d'effectuer des op�rations sur des tableaux (Array) de type primitif.
 * 
 * @author Simon V�zina
 * @since 2017-12-15
 * @version 2018-05-25 (Version labo 1.0 : Les fonctions discr�tes)
 */
public final class SArrays {

  /**
   * M�thode pour d�terminer si deux tableau de nombres de type double sont <b>relativement �gaux</b>. 
   * En utilisant une approche de calcul de diff�rence, on v�rifie si pour chaque �l�ment du tableau
   * <ul>a - b < EPSILON*ref</ul>  
   * afin de <b>valid� l'�galit�</b> entre a et b (a == b). EPSILON est un seuil de pr�cision 
   * et ref est une base de r�f�rence (la valeur absolue la plus �lev�e parmis a et b).
   * <p>Cenpendant, si les deux chiffres sont inf�rieurs � EPSILON, ils seront consid�r�s comme �gaux.</p>
   * 
   * @param tab1 Le premier tableau � comparer.
   * @param tab2 Le deuxi�me tableau � comparer.
   * @param epsilon La pr�cision acceptable.
   * @return <b>true</b> si les deux tableaux sont <b>relativement �gaux</b> et <b>false</b> sinon.
   */
  public static boolean nearlyEquals(double[] tab1, double[] tab2, double epsilon)
  {
    // V�rifier que les deux tableaux ont la m�me taille.
    if(tab1.length != tab2.length)
      return false;
    
    // V�rifier que l'ensemble des donn�e indexe par indexe sont "nearlyEquals".
    for(int i = 0; i < tab1.length; i++)
      if(!SMath.nearlyEquals(tab1[i], tab2[i], epsilon))
        return false;
    
    // Puisque l'ensemble des valeurs indexe par indexe sont "nearlyEquals", le tableau doit l'�tre. 
    return true;
  }
  
  /**
   * M�thode pour effectuer une copie d'un tableau.
   * 
   * @param tab Le tableau � copier.
   * @param copy Le tableau qui va recevoir la copie.
   * @throws SRuntimeException Si les deux tableaux n'ont pas la m�me longueur.
   */
  public static void copy(int[] tab, int[] copy) throws SRuntimeException
  {
    if(tab.length != copy.length)
      throw new SRuntimeException("Les deux tableaux n'ont pas la m�me longueur (" + tab.length + " et " + copy + ").");
    
    for(int i = 0; i < tab.length; i++)
      copy[i] = tab[i];
  }
  
  /**
   * M�thode pour effectuer une copie d'un tableau.
   * 
   * @param tab Le tableau � copier.
   * @param copy Le tableau qui va recevoir la copie.
   * @throws SRuntimeException Si les deux tableaux n'ont pas la m�me longueur.
   */
  public static void copy(int[] tab, byte[] copy) throws SRuntimeException
  {
    if(tab.length != copy.length)
      throw new SRuntimeException("Les deux tableaux n'ont pas la m�me longueur (" + tab.length + " et " + copy + ").");
    
    for(int i = 0; i < tab.length; i++)
      copy[i] = (byte) tab[i];
  }
  
  /**
   * M�thode pour effectuer une copie d'un tableau.
   * 
   * @param tab Le tableau � copier.
   * @param copy Le tableau qui va recevoir la copie.
   * @throws SRuntimeException Si les deux tableaux n'ont pas la m�me longueur.
   */
  public static void copy(byte[] tab, byte[] copy) throws SRuntimeException
  {
    if(tab.length != copy.length)
      throw new SRuntimeException("Les deux tableaux n'ont pas la m�me longueur (" + tab.length + " et " + copy + ").");
    
    for(int i = 0; i < tab.length; i++)
      copy[i] = (byte) tab[i];
  }
  
  /**
   * M�thode pour effectuer une copie d'un tableau.
   * 
   * @param tab Le tableau � copier.
   * @param copy Le tableau qui va recevoir la copie.
   * @throws SRuntimeException Si les deux tableaux n'ont pas la m�me longueur.
   */
  public static void copy(double[] tab, double[] copy) throws ArrayIndexOutOfBoundsException
  {
    if(tab.length != copy.length)
      throw new ArrayIndexOutOfBoundsException("Les deux tableaux n'ont pas la m�me longueur (" + tab.length + " et " + copy + ").");
    
    for(int i = 0; i < tab.length; i++)
      copy[i] = tab[i];
  }
  
  /**
   * M�thode pour r�aliser <u>l'addition</u> d'une valeur � un tableau.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab Le 1er tableau.
   * @param a La valeur � ajouter.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   */
  public static double[] add(double[] tab, double a) 
  {
    double[] result = new double[tab.length];
    
    for(int i = 0; i < tab.length; i++)
      result[i] = tab[i] + a;
    
    return result;
  }
  
  /**
   * M�thode pour r�aliser <u>l'addition</u> entre deux tableaux de m�me dimension.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab1 Le 1er tableau.
   * @param tab2 Le 2e tableau.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   * @throws ArrayIndexOutOfBoundsException Si les deux tableaux n'ont pas la m�me taille.
   */
  public static double[] add(double[] tab1, double[] tab2) throws ArrayIndexOutOfBoundsException
  {
    // V�rification de la taille des tableaux.
    if(tab1.length != tab2.length)
      throw new ArrayIndexOutOfBoundsException("Les deux tableaux n'ont pas la m�me longueur (" + tab1.length + " et " + tab2 + ").");
    
    double[]res = new double[tab1.length];
    for(int i=0;i<tab1.length;i++) {
    	res[i]= tab1[i]+tab2[i];
    	
    }
    
    return res;
  }
  
  
  /**
   * M�thode pour r�aliser <u>la soustraction</u> d'une valeur � un tableau.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab Le 1er tableau.
   * @param a La valeur � soustraire.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   */
  public static double[] substract(double[] tab, double a) 
  {
    return add(tab, -1*a);
  }
  
  /**
   * M�thode pour r�aliser <u>la soustraction</u> entre deux tableaux de m�me dimension.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab1 Le 1er tableau.
   * @param tab2 Le 2e tableau.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   * @throws ArrayIndexOutOfBoundsException Si les deux tableaux n'ont pas la m�me taille.
   */
  public static double[] substract(double[] tab1, double[] tab2) throws ArrayIndexOutOfBoundsException
  {
    if(tab1.length != tab2.length)
      throw new ArrayIndexOutOfBoundsException("Les deux tableaux n'ont pas la m�me longueur (" + tab1.length + " et " + tab2 + ").");
    
    double[]res = new double[tab1.length];
    for(int i=0;i<tab1.length;i++) {
    	res[i]= tab1[i]-tab2[i];
    	
    }
    
    return res;
    
  }
  
  /**
   * M�thode pour r�aliser <u>la multiplication</u> d'une valeur � un tableau.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab Le 1er tableau.
   * @param a La valeur � multiplier.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   */
  public static double[] multiply(double[] tab, double a) 
  {
	  double[]res = new double[tab.length];
	    for(int i=0;i<tab.length;i++) {
	    	res[i]= tab[i]*a;
	    	
	    }
	    
	    return res;
  }
  
  /**
   * M�thode pour r�aliser <u>la multiplication</u> entre deux tableaux de m�me dimension.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab1 Le 1er tableau.
   * @param tab2 Le 2e tableau.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   * @throws ArrayIndexOutOfBoundsException Si les deux tableaux n'ont pas la m�me taille.
   */
  public static double[] multiply(double[] tab1, double[] tab2) throws ArrayIndexOutOfBoundsException
  {
    if(tab1.length != tab2.length)
      throw new ArrayIndexOutOfBoundsException("Les deux tableaux n'ont pas la m�me longueur (" + tab1.length + " et " + tab2 + ").");
    
    double[]res = new double[tab1.length];
    for(int i=0;i<tab1.length;i++) {
    	res[i]= tab1[i]*tab2[i];
    	
    }
    
    return res;
  }
  
  /**
   * M�thode pour r�aliser <u>le produit scalaire</u> entre deux tableaux de m�me dimension.
   * 
   * @param A Le 1er tableau.
   * @param B Le 2e tableau.
   * @return Le r�sultat de l'op�ration math�matique.
   */
  public static double dot(double[] A, double[] B)
  {
    throw new SNoImplementationException("La m�thode n'a pas �t� impl�ment�e.");
  }
  
  /**
   * M�thode pour r�aliser <u>la division</u> entre deux tableaux de m�me dimension.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab1 Le 1er tableau.
   * @param tab2 Le 2e tableau.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   * @throws ArrayIndexOutOfBoundsException Si les deux tableaux n'ont pas la m�me taille.
   */
  public static double[] divide(double[] tab1, double[] tab2) throws ArrayIndexOutOfBoundsException
  {
    if(tab1.length != tab2.length)
      throw new ArrayIndexOutOfBoundsException("Les deux tableaux n'ont pas la m�me longueur (" + tab1.length + " et " + tab2 + ").");
    
    double[]res = new double[tab1.length];
    for(int i=0;i<tab1.length;i++) {
    	res[i]= tab1[i]/tab2[i];
    	
    }
    
    return res;
  }
  
  /**
   * M�thode pour r�aliser <u>l'exponentiel</u> d'un tableau.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab Le tableau.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   */
  public static double[] exp(double[] tab)
  {
    double[] result = new double[tab.length];
    
    for(int i = 0; i < tab.length; i++)
      result[i] = Math.exp(tab[i]);
    
    return result;
  }
  
  /**
   * M�thode pour r�aliser <u>la puissance</u> d'un tableau.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab Le tableau.
   * @param n La puissance.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   */
  public static double[] pow(double[] tab, double n)
  {
	  double[]res = new double[tab.length];
	    for(int i=0;i<tab.length;i++) {
	    	res[i]= Math.pow(tab[i], n);
	    	
	    }
	    
	    return res;
  }
  
  /**
   * M�thode pour r�aliser <u>le sinus</u> d'un tableau. Les valeurs du tableau seront consid�r�es en <b>radian</b>.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab Le tableau.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   */
  public static double[] sin(double[] tab)
  {
    double[] result = new double[tab.length];
    
    for(int i = 0; i < tab.length; i++)
      result[i] = Math.sin(tab[i]);
    
    return result;
  }
  
  /**
   * M�thode pour r�aliser <u>le cosinus</u> d'un tableau. Les valeurs du tableau seront consid�r�es en <b>radian</b>.
   * Un nouveau tableau sera retourn� contenant le r�sultat de l'op�ration math�matique.
   * 
   * @param tab Le tableau.
   * @return Le tableau contenant le r�sultat de l'op�ration math�matique.
   */
  public static double[] cos(double[] tab)
  {
    double[] result = new double[tab.length];
    
    for(int i = 0; i < tab.length; i++)
      result[i] = Math.cos(tab[i]);
    
    return result;
  }
  
    
  /**
   * M�thode pour obtenir la <b>plus grande valeur</b> d'un tableau.
   * 
   * @param tab Le tableau.
   * @return La plus grande valeur du tableau.
   */
  public static int findMax(int[] tab)
  {
    int max = tab[0]; 
    
    for(int v : tab)
      if(v > max)
        max = v;
    
    return max;
  }
  
  /**
   * M�thode pour obtenir la <b>plus grande valeur</b> d'un tableau.
   * 
   * @param tab Le tableau.
   * @return La plus grande valeur du tableau.
   */
  public static double findMax(double[] tab)
  {
    double max = tab[0]; 
    
    for(double v : tab)
      if(v > max)
        max = v;
    
    return max;
  }
  
  /**
   * M�thode pour obtenir l'indice dans le tableau de la <b>valeur la plus grande</b>.
   * 
   * @param tab Le tableau.
   * @return L'indice dans le tableau o� l'on trouve la valeur la plus grande.
   */
  public static int findIndexOfMax(double[] tab)
  {
    // Choisir la meilleure cote.
    int index = 0;
    double value = tab[index];
    
    // Parcourir le tableau � la recherche de la plus grande valeur.
    for(int i = 1; i < tab.length; i++)
      if(tab[i] > value)
      {
        value = tab[i];
        index = i;
      }
    
    return index;
  }
  
  /**
   * M�thode pour obtenir la <b>plus petite valeur</b> d'un tableau.
   * 
   * @param tab Le tableau.
   * @return La plus petite valeur du tableau.
   */
  public static int findMin(int[] tab)
  {
    int min = tab[0]; 
    
    for(int v : tab)
      if(v < min)
        min = v;
    
    return min;
  }
  
  /**
   * M�thode pour obtenir la <b>plus petite valeur</b> d'un tableau.
   * 
   * @param tab Le tableau.
   * @return La plus petite valeur du tableau.
   */
  public static double findMin(double[] tab)
  {
    double min = tab[0]; 
    
    for(double v : tab)
      if(v < min)
        min = v;
    
    return min;
  }
  
  /**
   * M�thode pour convertir un tableau d'entier vers un tableau de nombre r�el entre une valeur minimale et maximale.
   * La correspondance entre les valeurs converties sera lin�aire.
   * 
   * @param data Les entiers � convertir.
   * @param min La borne minimale.
   * @param max La borne maximale.
   * @return Le tableau converti.
   * @throws SRuntimeException Si les bornes sont mal d�finies.
   */
  public static double[] mappingIntToDouble(int[] data, double min, double max) throws SRuntimeException
  {
    if(min > max)
      throw new SRuntimeException("Erreur SMath 003 : La borne minimale " + min + " et la borne maximale " + max + " sont mal d�finies.");
    
    int min_value = findMin(data);
    int max_value = findMax(data);
    
    double[] result = new double[data.length];
    
    // It�rer sur l'ensemble des �l�ments du tableau.
    for(int i = 0; i < result.length; i++)
    {
      double t = SMath.reverseLinearInterpolation((double)data[i], (double)min_value, (double)max_value);
      result[i] = SMath.linearInterpolation(min, max, t);
    }
    
    return result;
  }
  
  /**
   * M�thode pour convertir un tableau de nombre r�el vers un tableau d'entier entre une valeur minimale et maximale.
   * La correspondance entre les valeurs converties sera lin�aire.
   * 
   * @param data Les donn�es � convertir.
   * @param min La borne minimale.
   * @param max La borne maximale.
   * @return Le tableau converti.
   * @throws IllegalArgumentException Si les bornes sont mal d�finies.
   */
  public static int[] mappingDoubleToInt(double[] data, int min, int max) throws IllegalArgumentException
  {
    return mappingDoubleToInt(data, findMin(data), findMax(data), min, max);
  }
  
  /**
   * ...
   * 
   * @param data
   * @param double_min
   * @param double_max
   * @param int_min
   * @param int_max
   * @return
   * @throws IllegalArgumentException
   */
  public static int[] mappingDoubleToInt(double[] data, double double_min, double double_max, int int_min, int int_max) throws IllegalArgumentException
  {
    if(int_min > int_max)
      throw new IllegalArgumentException("Erreur SArrayUtil 004 : La borne enti�re minimale " + int_min + " et la borne enti�re maximale " + int_max + " sont mal d�finies.");
    
    if(double_min > double_max)
      throw new IllegalArgumentException("Erreur SArrayUtil 004 : La borne double minimale " + double_min + " et la borne double maximale " + double_max + " sont mal d�finies.");
       
    int[] result = new int[data.length];
    
    // It�rer sur l'ensemble des �l�ments du tableau.
    for(int i = 0; i < result.length; i++)
    {
      double t = SMath.reverseLinearInterpolation(data[i], double_min, double_max);
      result[i] = (int)SMath.linearInterpolation(int_min, int_max, t);
    }
    
    return result;
  }
  
  /**
   * <p>
   * M�thode permettant de g�n�rer un tableau contenant les �l�ments identique de deux tableaux de valeur <ul>pr�alablement tri�</ul>.
   * </p>
   * 
   * <p>
   * <b>REMARQUE</b> : Le fonctionnement de cette m�thode ne sera pas valide si les deux tableaux pass�s en param�tre ne sont pas p�alablement tri�.
   * </p>
   * R�f�rence : https://stackoverflow.com/questions/32676381/find-intersection-of-two-arrays
   * 
   * @param tab1 Le 1ier tableau � comparer.
   * @param tab2 Le 2i�me tableau � comparer.
   * @return Un tableau comprenant les �l�ments identiques de deux tableaux (l'intersection des deux tableaux).
   */
  public static double[] intersectionSortedArray(double[] tab1, double[] tab2)
  {
    return intersectionSortedArray(tab1, tab2, SMath.EPSILON);
  }
  
  /**
   * <p>
   * M�thode permettant de g�n�rer un tableau contenant les �l�ments identique de deux tableaux de valeur <ul>pr�alablement tri�</ul>.
   * </p>
   * 
   * <p>
   * <b>REMARQUE</b> : Le fonctionnement de cette m�thode ne sera pas valide si les deux tableaux pass�s en param�tre ne sont pas p�alablement tri�.
   * </p>
   * R�f�rence : https://stackoverflow.com/questions/32676381/find-intersection-of-two-arrays
   * 
   * @param tab1 Le 1ier tableau � comparer.
   * @param tab2 Le 2i�me tableau � comparer.
   * @param epsilon La pr�cision de la comparaison.
   * @return Un tableau comprenant les �l�ments identiques de deux tableaux (l'intersection des deux tableaux).
   */
  public static double[] intersectionSortedArray(double[] tab1, double[] tab2, double epsilon)
  {
    double intersection[] = new double[Math.min(tab1.length, tab2.length)];
    int count = 0;

    int i = 0; int j = 0;
    while (i < tab1.length && j < tab2.length)
    {
      // V�rifier s'il y a �galit�.
      if(SMath.nearlyEquals(tab1[i], tab2[j], epsilon)) 
      {
        intersection[count] = tab1[i];
        count++;
        i++;
        j++;
      }
      else
        // Avancer dans la recherche des �l�ments.
        if (tab1[i] < tab2[j]) 
          i++;                    // avancer dans le tableau 1
        else
          j++;                    // avancer dans le tableau 2
    }

    // Construire un tableau avec l'espace m�moire minimum.
    double[] result = new double[count];
    for(int k = 0; k < result.length; k++)
      result[k] = intersection[k];
    
    return result;
  }
  
  /**
   * M�thode pour changer la dimension d'un tableau. Si la nouvelle taille est inf�rieure � la pr�c�dente, les derniers �l�ments ne seront pas copi�.
   * Si la nouvelle taille est sup�rieure � la pr�c�dente, des valeurs nulles sont attribu�es aux positions sup�rieures. 
   * 
   * @param array Le tableau � copier.
   * @param new_size La nouvelle dimension du tableau.
   * @return Un tableau avec une copie des donn�es avec une taille diff�rente.
   */
  public static double[] resize(double[] array, int new_size)
  {
      // Remplir un nouveau tableau ayant seulement la taille demand�.
      // Tous les �l�ments du tableau pr�c�dent 
      double[] result = new double[new_size];
      
      for(int i = 0; i < new_size; i++)
        result[i] = array[i];
     
      return result;
  }
  
  /**
   * M�thode pour changer la dimension d'un tableau. Si la nouvelle taille est inf�rieure � la pr�c�dente, les derniers �l�ments ne seront pas copi�.
   * Si la nouvelle taille est sup�rieure � la pr�c�dente, des valeurs nulles sont attribu�es aux positions sup�rieures. 
   * 
   * @param array Le tableau � copier.
   * @param new_size La nouvelle dimension du tableau.
   * @return Un tableau avec une copie des donn�es avec une taille diff�rente.
   */
  public static int[] resize(int[] array, int new_size)
  {
      // Remplir un nouveau tableau ayant seulement la taille demand�.
      // Tous les �l�ments du tableau pr�c�dent 
      int[] result = new int[new_size];
      
      for(int i = 0; i < new_size; i++)
        result[i] = array[i];
     
      return result;
  }
  
  /**
   * M�thode pour faire l'�criture d'un tableau dans un fichier texte.
   * 
   * @param file_name Le nom du fichier.
   * @param values Le tableau des valeurs � �crire.
   */
  public static void write(String file_name, double[] values)
  {
    try{
      
      FileWriter fw = new FileWriter(file_name);
      BufferedWriter bw = new BufferedWriter(fw);
    
      // Faire l'�criture de chaque valeur et changer de ligne.
      for(int i = 0; i < values.length; i++)
      {
        bw.write(Double.toString(values[i]));
        bw.newLine();
      }
      
      bw.close(); //  fermer celui-ci en premier, sinon, ERROR !!!
      fw.close();   
      
    }catch(IOException ioe){
      SLog.logWriteLine("Message SArrays - Une erreur de type I/O est survenue.");
      ioe.printStackTrace();
    }
  }
  
}
