/**
 * 
 */
package sim.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * La classe <b>SBrowser</b> repr�sente un explorateur de r�pertoire et de fichier.
 * 
 * @author Simon V�zina
 * @since 2018-02-26
 * @version 2018-03-14
 */
public class SBrowser {

  /**
   * La constante <b>DEFAULT_MAX_DEPTH</b> repr�sente le niveau de profondeur maximal de recherche r�cursive dans les sous-r�pertoire.
   */
  private final int DEFAULT_MAX_DEPTH = 8;
  
  /**
   * La variable <b>file</b> repr�sente l'emplacement courant de l'explorateur.
   */
  private File file;
  
  /**
   * Constructeur d'un explorateur de r�pertoire.
   */
  public SBrowser()
  {
    this.file = new File(System.getProperty("user.dir"));
  }
  
  /**
   * M�thode pour obtenir le nom du r�pertoire o� est situ� l'explorateur de r�pertoire.
   * 
   * @return
   */
  public String getDirectoryName()
  {
    return this.file.getName();
  }
  
  /**
   * M�thode pour obtenir le chemin o� est situ� l'explorateur de r�pertoire.
   * 
   * @return Le chemin de localisation de l'explorateur.
   */
  public String getPath()
  {
    return this.file.getPath();
  }
  
  /**
   * M�thode qui retourne une liste d'acc�s au fichier visible par la localisation courant de l'explorateur.
   * 
   * @return Un liste d'acc�s aux fichiers.
   */
  public List<File> getFileList()
  {
    List<File> list = new ArrayList<File>();
    
    for(File f : file.listFiles())
      if(f.isFile())
        list.add(f);
    
    return list;
  }
  
  /**
   * M�thode qui retourne une liste d'acc�s au r�pertoire visible par la localisation courant de l'explorateur.
   * 
   * @return Un liste d'acc�s aux r�pertoires.
   */
  public List<File> getDirectoryList()
  {
    List<File> list = new ArrayList<File>();
    
    for(File f : file.listFiles())
      if(f.isDirectory())
        list.add(f);
    
    return list;
  }
  
  /**
   * M�thode pour entrer dans un r�pertoire.
   * 
   * @param directory_name Le nom du r�pertoire dans lequel on veut entrer.
   * @throws FileNotFoundException Si le nom du r�pertoire n'a pas �t� trouv�.
   * @throws SManyFilesFoundException Si plusieurs r�pertoires ayant le m�me nom a �t� trouv�s.
   */
  public void enter(String directory_name) throws FileNotFoundException, SManyFilesFoundException
  {
    List<File> list = getDirectoryList();
    List<File> found_list = new ArrayList<File>();
    
    for(File f : list)
      if(f.getName().equalsIgnoreCase(directory_name))
        found_list.add(f);
    
    if(found_list.isEmpty())
      throw new FileNotFoundException("Erreur SBrowser 001 : Le r�pertoire '" + directory_name + "' n'a pas �t� trouv� � partir de l'emplacement initiale '" + getPath() + "'.");
    
    if(found_list.size() > 1)
      throw new SManyFilesFoundException("Erreur SBrowser 002 : Le r�pertoire '" + directory_name + "' a �t� trouv� � plusieurs reprise � partir de l'emplacement initiale '" + getPath() + "'. Voici la liste : " + found_list);
    
    // Affectation de la nouvelle position de l'explorateur.
    this.file = found_list.get(0);
  }
  
  /**
   * M�thode pour sortir d'un r�pertoire.
   */
  public void back()
  {
    this.file = new File(file.getParent());
  }
  
  /**
   * M�thode pour d�placer l'explorateur � l'emplacement d'un r�pertoire � partir du r�pertoire courant de l'explorateur.
   * 
   * @param directory_name Le nom du r�pertoire vers lequel l'explorateur se dirigera.
   * @throws FileNotFoundException Si le nom du r�pertoire n'a pas �t� trouv�.
   * @throws SManyFilesFoundException Si plusieurs r�pertoires ayant le m�me nom a �t� trouv�s.
   */
  public void goTo(String directory_name) throws FileNotFoundException, SManyFilesFoundException
  {
    goTo(directory_name, DEFAULT_MAX_DEPTH);
  }
  
  /**
   * M�thode pour d�placer l'explorateur � l'emplacement d'un r�pertoire � partir du r�pertoire courant de l'explorateur.
   * 
   * @param directory_name Le nom du r�pertoire vers lequel l'explorateur se dirigera.
   * @throws FileNotFoundException Si le nom du r�pertoire n'a pas �t� trouv�.
   * @throws SManyFilesFoundException Si plusieurs r�pertoires ayant le m�me nom a �t� trouv�s.
   */
  public void goTo(String directory_name, int max_depth) throws FileNotFoundException, SManyFilesFoundException
  {
    List<File> list = new ArrayList<File>();
    
    searchDirectories(directory_name, file, 0, max_depth, list);
    
    if(list.isEmpty())
      throw new FileNotFoundException("Erreur SBrowser 003 : Le r�pertoire '" + directory_name + "' n'a pas �t� trouv� � partir de l'emplacement initiale '" + getPath() + "'.");
    
    if(list.size() > 1)
      throw new SManyFilesFoundException("Erreur SBrowser 004 : Le r�pertoire '" + directory_name + "' a �t� trouv� � plusieurs reprise � partir de l'emplacement initiale '" + getPath() + "'. Voici la liste : " + list);
    
    // Affectation de la nouvelle position de l'explorateur.
    this.file = list.get(0);
  }
  
  /**
   * M�thode pour obtenir une liste d'acc�s � un fichier correspondant � un nom particulier.
   * 
   * @param file_name Le nom du fichier en recherche.
   * @return Une liste de fichier ayant le nom d�sir�.
   */
  public List<File> searchFiles(String file_name)
  {
    List<File> list = new ArrayList<File>();
    
    searchFiles(file_name, file, 0, DEFAULT_MAX_DEPTH, list);
    
    return list;
  }
  
  /**
   * M�thode pour obtenir un acc�s � un fichier correspondan � un nom particulier.
   * 
   * @param file_name Le nom du fichier en recherche.
   * @return Un acc�s � un fichier ayant le nom d�sir�.
   * @throws FileNotFoundException Si le fichier n'a pas �t� trouv�.
   * @throws SManyFilesFoundException Si plusieurs fichiers ayant le m�me nom a �t� trouv�s.
   */
  public File findFile(String file_name) throws FileNotFoundException, SManyFilesFoundException
  {
    List<File> list = searchFiles(file_name);
    
    if(list.isEmpty())
      throw new FileNotFoundException("Erreur SBrowser 003 : Le fichier '" + file_name + "' n'a pas �t� trouv� � partir de l'emplacement initiale '" + getPath() + "'.");
    
    if(list.size() > 1)
      throw new SManyFilesFoundException("Erreur SBrowser 004 : Le fichier '" + file_name + "' a �t� trouv� � pluseurs reprise � partir de l'emplacement initiale '" + getPath() + "'.");
    
    return list.get(0);
  }
  
  /**
   * M�thode pour faire la recherche d'un fichier d�sir� de fa�on r�cursive. � chaque fois que le fichier sera trouv�,
   * l'adresse du fichier sera sauvegard� dans la liste.
   * 
   * @param file_name Le nom du fichier en recherche.
   * @param current_file Le r�pertoire o� la recherche est rendue.
   * @param depth Le niveau de profondeur courant de la r�cursivit�.
   * @param max_depth Le niveau de profoncteur maximal de la r�cursivit�.
   * @param file_list La liste des fichies trouv�s.
   */
  private void searchFiles(final String file_name, final File current_file, final int depth, final int max_depth, final List<File> file_list)
  {
    // V�rification du niveau de profondeur maximal du fichier en recherche
    if(depth <= max_depth)
    {
      // V�rification si l'analyse se fait sur un r�pertoire et s'il est accessible.
      if(current_file.isDirectory() && current_file.canRead())                            
      {
        // It�rer sur l'ensemble du contenu du r�pertoire
        for(File temp : current_file.listFiles())                                 
        {
          // V�rifier si le fichier it�r� est un r�pertoire. Dans ce cas, on continue la recherche r�cursivement.  
          if(temp.isDirectory())                                         
            searchFiles(file_name, temp, depth + 1, max_depth, file_list); 
          else 
            if(temp.getName().equalsIgnoreCase(file_name))                // Si le fichier est trouv� ! 
                file_list.add(temp);    
        }
      } 
    }
  }
  
  /**
   * M�thode pour faire la recherche du r�pertoire d�sir� de fa�on r�cursive. � chaque fois que le r�pertoire sera trouv�,
   * l'adresse du r�pertoire sera sauvegard� dans la liste.
   * 
   * @param file Le r�pertoire o� la recherche est rendue.
   * @param depth Le niveau de profondeur de la r�cursivit�.
   */
  private void searchDirectories(final String directory_name, final File current_file, final int depth, final int max_depth, final List<File> directory_list)
  {
    // V�rification du niveau de profondeur maximal du fichier en recherche
    if(depth <= max_depth)
    {
      // V�rification si l'analyse se fait sur un r�pertoire et l'acc�s au r�pertoire est possible.
      if(current_file.isDirectory() && current_file.canRead())                                                  
      {                                            
        // V�rifier si le r�pertoire courant porte le nom recherch�.
        if(directory_name.equalsIgnoreCase(current_file.getName()))
          directory_list.add(current_file);
        
        // It�rer sur l'ensemble du contenu du r�pertoire et it�rer r�cursivement dans le cas d'un r�pertoire.
        for(File temp : current_file.listFiles())   
          if(temp.isDirectory())            
            searchDirectories(directory_name, temp, depth + 1, max_depth, directory_list);        
      }
    }
  }
  
}
