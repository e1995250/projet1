/**
 * 
 */
package sim.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import sim.exception.SConstructorException;

/**
 * La classe <b>SDirectorySearch</b> permet la recherche d'un nom de <u>r�pertoire</u> � partir d'un emplacement dans un ensemble de r�pertoire 
 * et de sous-r�pertoire de fa�on r�cursif.  
 * 
 * @author Simon V�zina
 * @since 2018-02-22
 * @version 2018-02-22
 */
public class SDirectorySearch {

  //-------------
  // CONSANTES //
  //-------------
  
  /**
   * La constante <b>DEFAULT_MAX_DEPTH<b> correspond au niveau de profondeur maximal de recherche par r�cursivit� �tant �gal � {@value}.
   */
  private final static int DEFAULT_MAX_DEPTH = 8;
  
//-------------
  // VARIABLES //
  //-------------
  
  /**
   * La variable <b>max_depth<b> correspond au niveau de profondeur de recherche par r�cursivit�.
   */
  private final int max_depth;
  
  /**
   * La variable <b>directory_name_to_search</b> correspond au r�pertoire � recherche. Ce nom peut comprendre "r�pertoire de localisation"/"nom du r�pertoire".
   * S'il y a un nom de r�pertoire d'inclu au nom du r�pertoire, cette information sera ajout�e � la variablle 'starting_subdirectory' lors de la recherche.
   */
  private final String directory_name_to_search;     
  
  /**
   * La variable <b>starting_subdirectory</b> correspond au nom de r�pertoire � partir du quel nous allons d�buter la recherche du fichier.
   * Il est important de pr�ciser que la recherche commencera <b>toujours</b> � partir du r�pertoire <b>"user.dir"</b>.
   */
  private final String starting_subdirectory;  
  
  /**
   * La variable <b>directory_found_list</b> correspond � la liste des noms de r�pertoire (r�pertoire/r�pertoire) o� le r�pertoire en recherche a �t� trouv�.
   */
  private final List<String> directory_found_list;    
 
  /**
   * Constructeur d'un chercheur de fichier � partir du r�pertoire "user.dir" o� l'application est lanc�e.
   *
   * @param directory_name Le nom du r�pertoire � chercher.
   * @throws SConstructorException Si le sous-r�pertoire ne permet pas d'identifier un r�pertoire valide.
   * @see System.getProperty("user.dir")
   */
  public SDirectorySearch(String directory_name) throws SConstructorException
  {
    this("",directory_name);
  }
  
  /**
   * Constructeur d'un chercheur de fichier � partir du r�pertoire "user.dir" o� l'application est lanc�e
   * et d'un sous-r�pertoire. Le recherche par r�cursivit� aura une profondeur maximal par d�faut.
   * 
   * @param subdirectory Le nom du sous-r�pertoire o� d�butera la recherche � partir du r�pertoire "user.dir". 
   * @param directory_name Le nom du r�pertoire � chercher.
   * @throws SConstructorException Si le sous-r�pertoire ne permet pas d'identifier un r�pertoire valide.
   * @see System.getProperty("user.dir")
   */
  public SDirectorySearch(String subdirectory, String directory_name) throws SConstructorException
  {
    this(subdirectory, directory_name, DEFAULT_MAX_DEPTH);
  }
  
  /**
   * Constructeur d'un chercheur de fichier � partir du r�pertoire "user.dir" o� l'application est lanc�e
   * et d'un sous-r�pertoire.
   * 
   * @param subdirectory Le nom du sous-r�pertoire o� d�butera la recherche � partir du r�pertoire "user.dir". 
   * @param directory_name Le nom du r�pertoire � chercher.
   * @throws SConstructorException Si le sous-r�pertoire ne permet pas d'identifier un r�pertoire valide.
   * @see System.getProperty("user.dir")
   */
  public SDirectorySearch(String subdirectory, String directory_name, int depth) throws SConstructorException
  {
    // V�rification du niveau de profondeur.
    if(depth < 1)
      throw new SConstructorException("Erreur SDirectorySearch 001 : Le niveau de profondeur '" + depth + "' doit �tre sup�rieur � 0.");
    
    this.max_depth = depth;
    
    // Mettre le sous-r�pertoire en minuscule
    //subdirectory = subdirectory.toLowerCase();
    
    // Obtenir le nom du fichier sans les informations de r�pertoire de localisation du fichier.
    this.directory_name_to_search = SStringUtil.getFileNameWithoutDirectory(directory_name.toLowerCase());  
        
    // �tablir le nom du r�pertoire initiale de recherche d�butant par le r�pertoire "user.dir" (l� o� l'application est lanc�e)
    if(subdirectory.equals(""))
      this.starting_subdirectory = System.getProperty("user.dir");
    else
      this.starting_subdirectory = System.getProperty("user.dir") + SStringUtil.FILE_SEPARATOR_CARACTER + subdirectory;
    
    // Liste des fichiers trouv�s, car il peut y avoir plusieurs fichiers avec le m�me nom
    this.directory_found_list = new ArrayList<String>();        
    
    // Cr�ation du r�pertoire o� d�but la recherche
    File directory = new File(starting_subdirectory); 
    
    // V�rification de la validit� du nom du r�pertoire
    if(directory.isDirectory())
      search(directory, 0);        //d�but de la recherche
    else
      throw new SConstructorException("Erreur SDirectorySearch 001 : Le sous-r�pertoire '" + subdirectory + "' n'est pas un r�pertoire dans l'adressage complet '" + starting_subdirectory + "'.");
  }
  
  /**
   * M�thode pour obtenir le nom du r�pertoire � rechercher.
   * @return Le nom du r�pertoire recherch�.
   */
  public String getDirectoryNameToSearch()
  {
    return directory_name_to_search;
  }
 
  /**
   * M�thode pour d�terminer si le r�pertoire a �t� trouv�. Cependant, il est possible que le nom du r�pertoire ait �t� trouv� plus d'une fois.
   * @return <b>true</b> si le r�pertoire a �t� trouv� et <b>false</b> sinon.
   */
  public boolean isDirectoryFound()
  {
    return !directory_found_list.isEmpty();
  }
  
  /**
   * M�thode pour d�terminer si le r�pertoire a �t� trouv� <b>plus d'une fois</b>.
   * @return <b>true</b> si le r�pertoire a �t� trouv� plus d'une fois et <b>false</b> sinon. 
   */
  public boolean isManyDirectoryFound()
  {
    return directory_found_list.size() > 1;
  }
  
  /**
   * M�thode pour obtenir la liste des adresses o� l'on a trouv� le nom du fichier. La liste sera vide si le fichier n'a pas �t� trouv�.
   * @return La liste des adresses o� le fichier a �t� trouv�.
   */
  public List<String> getDirectoryFoundList()
  {
    return directory_found_list;
  }
 
  
  /**
   * M�thode pour faire la recherche du r�pertoire d�sir� de fa�on r�cursive. � chaque fois que le r�pertoire sera trouv�,
   * l'adresse du r�pertoire sera sauvegard� dans la liste.
   * 
   * @param file Le r�pertoire o� la recherche est rendue.
   * @param depth Le niveau de profondeur de la r�cursivit�.
   */
  private void search(File file, int depth)
  {
    // V�rification du niveau de profondeur maximal du fichier en recherche
    if(depth < max_depth)
    {
      if(file.isDirectory())                                                  // V�rification si l'analyse se fait sur un r�pertoire
        if(file.canRead())                                                    // V�rification si l'acc�s au r�pertoire est possible
          if(directory_name_to_search.equals(file.getName().toLowerCase()))   // Si le r�pertoire est trouv� ! 
            directory_found_list.add(file.getAbsolutePath().toString());
          else
            for(File temp : file.listFiles())   // It�rer sur l'ensemble du contenu du r�pertoire
              if(temp.isDirectory())            // V�rifier si le fichier it�r� est un r�pertoire
                search(temp, depth + 1);        // Lecture r�cursive du r�pertoire de niveau sup�rieur
    }
  }
  
}// fin de la classe SDirectorySearch
