/**
 * 
 */
package sim.util;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * L'interface <b>SWriteable</b> repr�sente un objet pouvant �crire ses param�tres dans un fichier txt.
 * 
 * @author Simon V�zina
 * @since 2015-01-09
 * @version 2018-01-16
 */
public interface SWriteable {

	
  
  /**
	 * M�thode pour �crire un objet <b>SWriteable</b> dans un fichier texte en utilisant un <b>BufferedWriter</b>.
	 * 
	 * @param bw Le buffer pour l'�criture.
	 * @throws IOException Si une erreur de type I/O a �t� lanc�e par l'objet BufferedWriter.
	 */
	public void write(BufferedWriter bw) throws IOException;
	
	
	/*
	public void writeInformation(BufferedWriter bw) throws IOException;
  
	public void write() throws IOException;
	*/
	
}//fin interface SWriteable
